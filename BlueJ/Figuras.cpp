
#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada */
int main(){
    enum {
        triangulo,
        cuadrilatero,
        pentagono
    };
    int lados;
    double izquierdo,
           derecho,
           arriba,
           abajo,
           ang1, ang2, ang3;
    char angulo;

    printf("Cuantos lados tiene tu figura (MAX5, MIN3):  \n");
    scanf(" %i", &lados);

    if (lados == 3)
        lados = triangulo;
    else {
        if (lados == 4)
            lados = cuadrilatero;
        else {
            if (lados == 5)
                lados = pentagono;
            else {
                printf("te he dicho del 3 al 5!!\n");
                return main();
            }
        }
    }

    switch(lados) {
        case triangulo:
            printf("de que tamaños son los lados? \n");
            printf("lado izquierdo: \n");
            scanf(" %lf", &izquierdo);
            printf("lado derecho: \n");
            scanf(" %lf", &derecho);
            printf("lado abajo: \n");
            scanf(" %lf", &abajo);
            if (izquierdo == derecho && derecho == abajo)
                printf("equilatero\n");
            else{
                 if (izquierdo != derecho && izquierdo != abajo && abajo != derecho)
                     printf("escaleno\n");
                 else{ /*((izquierdo == derecho != abajo) || (izquierdo == abajo != derecho) || (abajo == derecho != izquierdo))*/
                        printf("¿Tiene uno de los angulos de 90º? (s/n)");
                        scanf("%s", &angulo);
                        if( angulo == 's')
                            printf("triangulo rectangulo\n");
                        else
                            printf("isosceles\n");
                 }
                 }
            break;

        case cuadrilatero:
            printf("cuadrilatero \n");
            break;

        case pentagono:
            printf("pentagono \n");
            break;
    }
    return EXIT_SUCCESS;
}
