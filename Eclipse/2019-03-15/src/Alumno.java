
public class Alumno {

	//DNI, NA, Nombre, Apellido, Sexo, Beca, Repetidor
	String dni;
	int na;
	String nombre;
	String apellido;
	char sexo;
	double beca;
	boolean repetidor;
	
	public Alumno(String dni, int na, String nombre, String apellido, char sexo, double beca, boolean repetidor) {
		super();
		this.dni = dni;
		this.na = na;
		this.nombre = nombre;
		this.apellido = apellido;
		this.sexo = sexo;
		this.beca = beca;
		this.repetidor = repetidor;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getNa() {
		return na;
	}

	public void setNa(int na) {
		this.na = na;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public double getBeca() {
		return beca;
	}

	public void setBeca(double beca) {
		this.beca = beca;
	}

	public boolean isRepetidor() {
		return repetidor;
	}

	public void setRepetidor(boolean repetidor) {
		this.repetidor = repetidor;
	}

	@Override
	public String toString() {
		return "Alumno [dni=" + dni + ", na=" + na + ", nombre=" + nombre + ", apellido=" + apellido + ", sexo=" + sexo
				+ ", beca=" + beca + ", repetidor=" + repetidor + "]";
	}
	
	
	
}
