
public class Camion extends Vehiculo {
	
	private double peso;
	private int numeroEjes;
	private boolean remolque;
	private double capacidad;
	private char señal;
	
	public Camion(String matricula, String marca, String modelo, double precio, double peso, int numeroEjes,
			boolean remolque, double capacidad, char señal) {
		super(matricula, marca, modelo, precio);
		this.peso = peso;
		this.numeroEjes = numeroEjes;
		this.remolque = remolque;
		this.capacidad = capacidad;
		this.señal = señal;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public int getNumeroEjes() {
		return numeroEjes;
	}
	public void setNumeroEjes(int numeroEjes) {
		this.numeroEjes = numeroEjes;
	}
	public boolean isRemolque() {
		return remolque;
	}
	public void setRemolque(boolean remolque) {
		this.remolque = remolque;
	}
	public double getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(double capacidad) {
		this.capacidad = capacidad;
	}
	public char getSeñal() {
		return señal;
	}
	public void setSeñal(char señal) {
		this.señal = señal;
	}
	@Override
	public String toString() {
		return "Camion [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", peso=" + peso + ", numeroEjes=" + numeroEjes + ", remolque=" + remolque + ", capacidad="
				+ capacidad + ", señal=" + señal + "]";
	}
	
	
}
