
public class Coche extends Vehiculo {

	//matricula, marca, modelo, precio, combustible, bastidor, ruedas, descapotable
	private char combustible; // 'G' gasolina, 'D' diesel, 'H' hibrido, 'E' electrico, 'O' otro
	private int bastidor;
	private int ruedas;
	private boolean descapotable;

	
	public Coche(String matricula, String marca, String modelo, double precio, char combustible, int bastidor,
			int ruedas, boolean descapotable) {
		super(matricula, marca, modelo, precio);
		this.combustible = combustible;
		this.bastidor = bastidor;
		this.ruedas = ruedas;
		this.descapotable = descapotable;
	}

	public char getCombustible() {
		return combustible;
	}

	public void setCombustible(char combustible) {
		this.combustible = combustible;
	}

	public int getBastidor() {
		return bastidor;
	}

	public void setBastidor(int bastidor) {
		this.bastidor = bastidor;
	}

	public int getRuedas() {
		return ruedas;
	}

	public void setRuedas(int ruedas) {
		this.ruedas = ruedas;
	}

	public boolean isDescapotable() {
		return descapotable;
	}

	public void setDescapotable(boolean descapotable) {
		this.descapotable = descapotable;
	}

	@Override
	public String toString() {
		return "Coche [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", combustible=" + combustible + ", bastidor=" + bastidor + ", ruedas=" + ruedas + ", descapotable="
				+ descapotable + "]";
	}
	
}