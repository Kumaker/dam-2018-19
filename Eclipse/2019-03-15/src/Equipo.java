
public class Equipo {
	String equipo;
	int abonados;
	double riqueza;
	boolean Primera; /*Juega o no en primera division*/
	char iniciales;
	
	public Equipo(String equipo, int abonados, double riqueza, boolean primera, char iniciales) {
		super();
		this.equipo = equipo;
		this.abonados = abonados;
		this.riqueza = riqueza;
		Primera = primera;
		this.iniciales = iniciales;
	}

	public String getEquipo() {
		return equipo;
	}

	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}

	public int getAbonados() {
		return abonados;
	}

	public void setAbonados(int abonados) {
		this.abonados = abonados;
	}

	public double getRiqueza() {
		return riqueza;
	}

	public void setRiqueza(double riqueza) {
		this.riqueza = riqueza;
	}

	public boolean isPrimera() {
		return Primera;
	}

	public void setPrimera(boolean primera) {
		Primera = primera;
	}

	public char getIniciales() {
		return iniciales;
	}

	public void setIniciales(char iniciales) {
		this.iniciales = iniciales;
	}

	@Override
	public String toString() {
		return "Equipo [equipo=" + equipo + ", abonados=" + abonados + ", riqueza=" + riqueza + ", Primera=" + Primera
				+ ", iniciales=" + iniciales + "]";
	}
			
	
}
