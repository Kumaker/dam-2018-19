
public class Moto extends Vehiculo {

	private int cilindrada;
	private int numeroluces;
	private boolean radio;
	
	public Moto(String matricula, String marca, String modelo, double precio, int cilindrada, int numeroluces,
			boolean radio) {
		super(matricula, marca, modelo, precio);
		this.cilindrada = cilindrada;
		this.numeroluces = numeroluces;
		this.radio = radio;
	}

	public int getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(int cilindrada) {
		this.cilindrada = cilindrada;
	}

	public int getNumeroluces() {
		return numeroluces;
	}

	public void setNumeroluces(int numeroluces) {
		this.numeroluces = numeroluces;
	}

	public boolean isRadio() {
		return radio;
	}

	public void setRadio(boolean radio) {
		this.radio = radio;
	}

	@Override
	public String toString() {
		return "Moto [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", cilindrada=" + cilindrada + ", numeroluces=" + numeroluces + ", radio=" + radio + "]";
	}
	
	
}
