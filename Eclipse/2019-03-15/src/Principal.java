
public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creacion de un objeto Coche
		/*
		 * Coche coche1 = new Coche(" 1234DFW", " Audi", " A3", 21340.65, 'D',
		 * 123459876, 5, false); Coche coche2 = new Coche(" 4487BCJ", " Opel", " Astra",
		 * 16000.45, 'G', 223455123, 5, true);
		 * 
		 * System.out.println(coche1); System.out.println(coche2.getPrecio());
		 * System.out.println(coche1.getCombustible());
		 * 
		 * coche1.setPrecio(15000); System.out.println(coche1.getPrecio());
		 * 
		 * Alumno alumno1 = new Alumno("49228091H", 334521, "Miguel Angel", "Lopez",
		 * 'H', 240.00, false); Alumno alumno2 = new Alumno("49228321C", 331121,
		 * "Juan Diego", "Wendy", 'H', 200.00, true); Alumno alumno3 = new
		 * Alumno("49228333D", 331111, "Alvaro", "Peluca", 'H', 100.00, true);
		 * 
		 * Coche [] arrayDeCoches = {coche1, coche2};
		 * 
		 * for (int i=0; i<arrayDeCoches.length; i++) {
		 * System.out.println(arrayDeCoches[i]); }
		 * 
		 * Alumno [] arrayDeAlumnos = {alumno1, alumno2, alumno3};
		 * 
		 * for (int i=arrayDeAlumnos.length-1; i>=0; i--) {
		 * System.out.println(arrayDeAlumnos[i]); }
		 */
		int[][] matriz = { { 1, 3, 0, -2, 7 },
						   { 4, 9, 9, 5, 6 },
						   { 8, 1, 3, 4, 5 } };
		
		// primer for recorre las filas
		for (int i = 0; i < matriz.length; i++) {
			// segundo for recorre columnas
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j]);
			}
			System.out.println();
		}
	}

}
