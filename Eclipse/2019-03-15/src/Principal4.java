import java.util.InputMismatchException;
import java.util.Scanner;

public class Principal4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int n =0;
		int recaudacion = 0;
		String matricula = "";
		do {
			System.out.println("Menú normalito:");
			System.out.println("---------------");
			System.out.println("1.Coche");
			System.out.println("2.Moto");
			System.out.println("3.Camion");
			System.out.println("4.Recaudación");
			System.out.println("5.Para salir");
			System.out.print("Elija una opción");
			
			//Crear objeto teclado de la clase Scanner
			Scanner teclado = new Scanner(System.in);
			try {
			n = teclado.nextInt();
			} catch (InputMismatchException error) { //se deja que el error salte para capturar la escepcion y el carch tiene que ir debajo de la linea que da el error 
				System.out.println("Introduzca la opcion de forma correcta");
			}
	
			
			switch (n) {
				case 1: System.out.println("Dime la matricula\n");
				    matricula = teclado.next();
				    Coche coche = new Coche("matricula", "Opel", "Kadet", 9000, 'D', 111, 4, false);
				    recaudacion += 5;
					break;
				case 2: System.out.println("Dime la matricula\n");
					matricula = teclado.next();
					Moto moto = new Moto("matricula", "Peugeot", "abanty", 300, 220, 3, false);
					recaudacion += 2;
					break;
				case 3: System.out.println("Dime la matricula\n");
					matricula = teclado.next();
					Camion camion = new Camion("matricula", "Volvo", "330V", 27000, 3000, 4, true, 900.20, 'F' );
					recaudacion += 8;
					break;
				case 4: System.out.println("Ver recaudacion\n");
					System.out.println(recaudacion);
				    break;
				case 5: System.out.println("salir\n");
					break;
				default: System.out.println(" Elección no válida\n");
					break;
			}
		}while(n!=5);
	}

}
