import java.net.*;
import java.io.*;

public class Traductor {
	public static void main(String[] args) throws Exception {

		String palabra = "casa";
		URL direccion = new URL("https://dictionary.cambridge.org/es/diccionario/espanol-ingles/"+palabra);
		String html = obtenerHTML(direccion);
		String palabraTraducida = cortarURL(html, palabra);
		System.out.println(palabraTraducida);
		/*
		 * String palabraTraducida = cortarURL(html);
		 * System.out.println(palabraTraducida);
		 */
		System.out.println(html);

	}

	public static String obtenerHTML(URL direccionWeb) throws Exception {

		BufferedReader in = new BufferedReader(new InputStreamReader(direccionWeb.openStream()));
		String inputLine, codigo = "";

		while ((inputLine = in.readLine()) != null)
			codigo += inputLine;

		in.close();

		return codigo;
	}

	
	public static String cortarURL (String html, String palabra) {
		int ini = html.indexOf("traducir" +palabra+": ")+11+palabra.length();
		int fin = html.indexOf(". ");
		String cortar = html.substring(ini, fin);
		return cortar;
	}
	 
	 
}
