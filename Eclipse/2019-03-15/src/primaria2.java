
public class primaria2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Sin herencia
		/*Coche coche1 = new Coche("1234CVH", "Volvo", "S60", 25000, 'D', 123, 5, false);
		Coche coche2 = new Coche("5678KNY", "Mercedes", "CLA", 50000, 'G', 344, 5, true);
		Coche coche3 = new Coche("6991GJR", "Kia", "Sportage", 20000, 'G', 520, 4, false);
		Coche coche4 = new Coche("3780FWX", "Opel", "Astra", 26000, 'H', 277, 5, false);
		Coche coche5 = new Coche("2298BBC", "Volvo", "FH16", 100000, 'D', 840, 6, true);
		Coche coche6 = new Coche("4001JSN", "Citroen", "Berlingo", 13000, 'G', 631, 2, true);
		Coche coche7 = new Coche("9303DFD", "Suzuki", "Ninja", 48000, 'G', 559, 2, true);
		Coche coche8 = new Coche("7762GHT", "Renault", "Clio", 16500, 'D', 215, 4, false);

		
		Coche[][] arrayDeCoches = new Coche[2][4];
		arrayDeCoches[1][1] = coche1;
		arrayDeCoches[0][3] = coche2;
		arrayDeCoches[0][0] = coche3;
		arrayDeCoches[1][2] = coche4;
		arrayDeCoches[0][1] = coche5;
		arrayDeCoches[1][0] = coche6;
		arrayDeCoches[0][2] = coche7;
		arrayDeCoches[1][3] = coche8;

		for (int i = 0; i < arrayDeCoches.length; i++) {
			for (int j = 0; j < arrayDeCoches[i].length; j++) {
				System.out.print(arrayDeCoches[i][j].getMarca()+" "+arrayDeCoches[i][j].getModelo()+"\t");
			}
			System.out.println();
		}
		System.out.println(coche1.calcularIVA());
		System.out.println(coche1.categoria());
		coche1.pincharRueda();
		System.out.println(coche1.getRuedas());
	*/
		// Con herencia
		
		Vehiculo vehiculo1 = new Coche("5678KNY", "Mercedes", "CLA", 50000, 'G', 344, 5, true);
		Vehiculo vehiculo2 = new Coche("6991GJR", "Kia", "Sportage", 20000, 'G', 520, 4, false);
		Vehiculo vehiculo3 = new Camion("3780FWX", "Opel", "Astra", 26000, 7.5, 4, true, 3500,'L');
		Vehiculo vehiculo4 = new Camion("2298BBC", "Volvo", "FH16", 100000, 8.3, 3, false, 1020, 'C');
		
		System.out.println(vehiculo1);
		System.out.println(vehiculo2);
		System.out.println(vehiculo3);
		System.out.println(vehiculo4);
		
		
		
	/*	System.out.println("Calcular area del rectangulo:\n");
		System.out.println("Introduzca la base:");
		double base = teclado.nextDouble();
		System.out.println("Introduce la altura");
		double altura = teclado.nextDouble();
		Rectangulo rec = new Rectangulo("Rojo", 'P', base, altura);
		System.out.println("El area es:" + rec.calcularArea());
		break;*/
		
		
		
		
		
		
		
		
		
	}	
}
