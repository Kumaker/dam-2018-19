import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Checkbox;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JPasswordField;
import java.awt.TextField;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.ListSelectionModel;
import java.awt.Toolkit;
import java.awt.Dialog.ModalExclusionType;
import com.toedter.calendar.JCalendar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JDateChooser;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GUI {

	private JFrame frmEncuesta;
	private JPasswordField password;
	private JTextField usuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmEncuesta.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEncuesta = new JFrame();
		frmEncuesta
				.setIconImage(Toolkit.getDefaultToolkit().getImage("/home/student/malopez/Descargas/pokemon_7025.png"));
		frmEncuesta.setResizable(false);
		frmEncuesta.setTitle("Encuesta");
		frmEncuesta.setBounds(100, 100, 942, 517);
		frmEncuesta.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEncuesta.getContentPane().setLayout(null);
		frmEncuesta.setLocationRelativeTo(null);

		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(
				new String[] { "MADRID\t", "BARCELONA", "VALENCIA", "SEVILLA", "BILBAO", "ZARAGOZA", "ALBACETE" }));
		comboBox.setBounds(407, 161, 118, 24);
		frmEncuesta.getContentPane().add(comboBox);

		JLabel lblSeleccioneCiudad = new JLabel("Seleccione ciudad");
		lblSeleccioneCiudad.setBounds(252, 166, 137, 15);
		frmEncuesta.getContentPane().add(lblSeleccioneCiudad);

		JRadioButton Alumno = new JRadioButton("Alumno");
		Alumno.setBounds(381, 193, 144, 23);
		frmEncuesta.getContentPane().add(Alumno);

		JRadioButton Profesor = new JRadioButton("Profesor");
		Profesor.setBounds(521, 193, 144, 23);
		frmEncuesta.getContentPane().add(Profesor);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(784, 293, 135, 175);
		frmEncuesta.getContentPane().add(scrollPane);

		JTextArea observaciones = new JTextArea();
		scrollPane.setViewportView(observaciones);
		observaciones.setLineWrap(true);

		password = new JPasswordField();
		password.setEnabled(false);
		password.setEchoChar('*');
		password.setBounds(407, 110, 200, 34);
		frmEncuesta.getContentPane().add(password);

		JLabel lblIntroduzcaSuContrasea = new JLabel("Introduzca su contraseña");
		lblIntroduzcaSuContrasea.setBounds(216, 110, 173, 15);
		frmEncuesta.getContentPane().add(lblIntroduzcaSuContrasea);

		usuario = new JTextField();

		usuario.setBounds(407, 64, 200, 34);
		frmEncuesta.getContentPane().add(usuario);
		usuario.setColumns(10);

		JLabel lblIntroduzcaSuNombre = new JLabel("Introduzca su nombre");
		lblIntroduzcaSuNombre.setBounds(216, 73, 173, 15);
		frmEncuesta.getContentPane().add(lblIntroduzcaSuNombre);

		JButton siguiente = new JButton("Siguiente");
		siguiente.setEnabled(false);
		siguiente.setBounds(631, 443, 114, 25);
		frmEncuesta.getContentPane().add(siguiente);

		JButton btnAtras = new JButton("Atras");
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAtras.setBounds(493, 443, 114, 25);
		frmEncuesta.getContentPane().add(btnAtras);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(804, 164, 115, 117);
		frmEncuesta.getContentPane().add(scrollPane_1);

		JList list = new JList();
		scrollPane_1.setViewportView(list);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setModel(new AbstractListModel() {
			String[] values = new String[] { "CINE", "FUTBOL", "VIDEOJUEGOS", "DORMIR", "DEPORTES", "VIAJAR",
					"COCINAR" };

			public int getSize() {
				return values.length;
			}

			public Object getElementAt(int index) {
				return values[index];
			}
		});

		JCalendar calendar = new JCalendar();
		calendar.setBounds(30, 237, 237, 159);
		frmEncuesta.getContentPane().add(calendar);

		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd-MM-yyyy");
		dateChooser.setBounds(493, 240, 108, 19);
		frmEncuesta.getContentPane().add(dateChooser);
		
		JCheckBox Terminos = new JCheckBox("Acepto terminos y servicios");
		
		Terminos.setBounds(523, 373, 222, 23);
		frmEncuesta.getContentPane().add(Terminos);
		
		JButton btnAceptar = new JButton("Aceptar");
		
		btnAceptar.setBounds(440, 281, 89, 23);
		frmEncuesta.getContentPane().add(btnAceptar);

		// eventos
		usuario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				password.setEnabled(true);
			}
		});

		frmEncuesta.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				Profesor.setSelected(true);
			}
		});
		
		Alumno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(frmEncuesta,
				"¿Realmente es un alumno?", "Confirma por favor",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE);
				
				if (respuesta == 0) {
					observaciones.setText("Eres Alumno");
					Alumno.setSelected(true);
				} else { 
					observaciones.setText("No eres Alumno");
					Alumno.setSelected(false);
				}
			}
		});
			Terminos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
				if(Terminos.isSelected()) {
					siguiente.setEnabled(true);
				} else {
					siguiente.setEnabled(false);
				}
				
				
						
						
				}
		});
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(!usuario.getText().isEmpty() && !password.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, "Hola "+usuario.getText());
			
				// SI NO --> Mensaje No OK
			} 
					else {
					JOptionPane.showMessageDialog(null, "Faltan datos");
				}
				}
			});
	}
}
