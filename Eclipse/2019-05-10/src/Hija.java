import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

public class Hija {

	private JFrame frmHija;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Hija window = new Hija();
					window.frmHija.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Hija() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHija = new JFrame();
		frmHija.setTitle("Hija");
		frmHija.setBounds(100, 100, 450, 300);
		frmHija.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmHija.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("IR A PADRE");
		btnNewButton.setBounds(127, 106, 173, 65);
		frmHija.getContentPane().add(btnNewButton);
	}
}
