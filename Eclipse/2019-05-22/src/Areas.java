import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Areas {
	String poligono = null;
	
	private JFrame frmPoligonos;
	private JFrame setFrmPoligonos;
	
	public JFrame getFrmPoligonos() {
		return frmPoligonos;
	}
	
	public void setFrmPoligonos (JFrame frmPoligonos) {
		this.setFrmPoligonos = frmPoligonos;
	}
	Cuadrado cuadrado = new Cuadrado();
	Triangulo triangulo = new Triangulo();
	Circulo circulo = new Circulo();
	Rectangulo rectangulo = new Rectangulo();
	Rombo rombo = new Rombo();
	Trapecio trapecio = new Trapecio();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Areas window = new Areas();
					window.frmPoligonos.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the application.
	 */
	public Areas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPoligonos = new JFrame();
		frmPoligonos.setResizable(false);
		frmPoligonos.setTitle("Poligonos");
		frmPoligonos.setBounds(100, 100, 356, 206);
		frmPoligonos.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPoligonos.getContentPane().setLayout(null);
		frmPoligonos.setLocationRelativeTo(null);
		
		JComboBox poligonos = new JComboBox();
		poligonos.setModel(new DefaultComboBoxModel(new String[] {" ","Triangulo", "Cuadrado", "Circulo","Rectangulo","Rombo","Trapecio"}));
		poligonos.setBounds(181, 45, 129, 20);
		frmPoligonos.getContentPane().add(poligonos);
		
		JLabel lblSeleccionaUnaFigura = new JLabel("Selecciona una figura");
		lblSeleccionaUnaFigura.setBounds(31, 47, 140, 17);
		frmPoligonos.getContentPane().add(lblSeleccionaUnaFigura);
		
		JButton siguiente = new JButton("Siguiente");
		siguiente.setBounds(117, 107, 94, 32);
		frmPoligonos.getContentPane().add(siguiente);
		
		JButton Salir = new JButton("Salir");
		Salir.setBounds(238, 112, 89, 23);
		frmPoligonos.getContentPane().add(Salir);
		
		// Eventos
		poligonos.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				
				if(arg0.getStateChange() == ItemEvent.SELECTED) {
					Object source = arg0.getSource();
					if (source instanceof JComboBox) {
						JComboBox cb = (JComboBox)source;
						String selectedItem = cb.getSelectedItem().toString();
						poligono = selectedItem;
					}
				}
			}
		});
		siguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (poligono == null) {
					JOptionPane.showMessageDialog(null, "Elige un poligono");
				} else {
					switch(poligono) {
						case("Triangulo"):
							triangulo.getFrmTriangulo().setVisible(true);
							frmPoligonos.setVisible(false);
						break;
						case("Cuadrado"):
							cuadrado.getFrmCuadrado().setVisible(true);
							frmPoligonos.setVisible(false);
						break;
						case("Circulo"):
							circulo.getFrmCirculo().setVisible(true);
							frmPoligonos.setVisible(false);
						break;
						case("Rectangulo"):
							rectangulo.getFrmRectangulo().setVisible(true);
							frmPoligonos.setVisible(false);
						break;
						case("Rombo"):
							rombo.getFrmRombo().setVisible(true);
							frmPoligonos.setVisible(false);
						break;
						case("Trapecio"):
							trapecio.getFrmTrapecio().setVisible(true);
							frmPoligonos.setVisible(false);
					}
				}
			}
		});
		Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmPoligonos.dispose();
			}
		});
		
	}
	

}

