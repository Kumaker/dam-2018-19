import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Circulo {

	private JFrame frmCirculo;
	private JTextField radio;
	
	public JFrame getFrmCirculo() {
		return frmCirculo;
	}

	public void setFrmCirculo(JFrame frmCirculo) {
		this.frmCirculo = frmCirculo;
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Circulo window = new Circulo();
					window.frmCirculo.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Circulo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCirculo = new JFrame();
		frmCirculo.setResizable(false);
		frmCirculo.setTitle("Circulo");
		frmCirculo.setBounds(100, 100, 350, 200);
		frmCirculo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCirculo.getContentPane().setLayout(null);
		
		radio = new JTextField();
		radio.setBounds(118, 71, 86, 20);
		frmCirculo.getContentPane().add(radio);
		radio.setColumns(10);
		
		JLabel lblDimeElRadio = new JLabel("Dime el radio");
		lblDimeElRadio.setBounds(22, 74, 86, 14);
		frmCirculo.getContentPane().add(lblDimeElRadio);
		
		JButton Calcular = new JButton("Calcular");
		Calcular.setBounds(227, 36, 89, 43);
		frmCirculo.getContentPane().add(Calcular);
		
		JButton Salir = new JButton("Salir");
		Salir.setBounds(227, 90, 89, 23);
		frmCirculo.getContentPane().add(Salir);
	
		//Eventos
		
		Calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double resultado = Math.PI*(Double.parseDouble(radio.getText()) * Double.parseDouble(radio.getText()));
				JOptionPane.showMessageDialog(null, "El area es: "+Double.toString(resultado));
			}
		});
		Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmCirculo.dispose();
			}
		});
	}
}
