import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Cuadrado {

	private JFrame frmCuadrado;
	
	public JFrame getFrmCuadrado() {
		return frmCuadrado;
	}

	public void setFrmCuadrado(JFrame frmCuadrado) {
		this.frmCuadrado = frmCuadrado;
	}
	
	
	private JTextField lado1;
	private JLabel lblDimeLosLados;
	private JButton Calcular;
	private JButton Salir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cuadrado window = new Cuadrado();
					window.frmCuadrado.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Cuadrado() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCuadrado = new JFrame();
		frmCuadrado.setResizable(false);
		frmCuadrado.setTitle("Cuadrado");
		frmCuadrado.setBounds(100, 100, 350, 200);
		frmCuadrado.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCuadrado.getContentPane().setLayout(null);
		
		lado1 = new JTextField();
		lado1.setBounds(143, 80, 34, 20);
		frmCuadrado.getContentPane().add(lado1);
		lado1.setColumns(10);
		
		lblDimeLosLados = new JLabel("Dime el lado");
		lblDimeLosLados.setBounds(32, 73, 101, 35);
		frmCuadrado.getContentPane().add(lblDimeLosLados);
		
		Calcular = new JButton("Calcular");
		Calcular.setBounds(242, 61, 92, 58);
		frmCuadrado.getContentPane().add(Calcular);
		
		Salir = new JButton("Salir");
		Salir.setBounds(245, 130, 89, 23);
		frmCuadrado.getContentPane().add(Salir);
		
		//Eventos
		
		Calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double resultado = Double.parseDouble(lado1.getText()) * Double.parseDouble(lado1.getText());
				JOptionPane.showMessageDialog(null, "El area es: "+Double.toString(resultado));
				
			}
		});
		Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmCuadrado.dispose();
			}
		});
	}

	
}
