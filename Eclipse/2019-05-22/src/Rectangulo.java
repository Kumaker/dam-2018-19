import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Rectangulo {

	public JFrame getFrmRectangulo() {
		return frmRectangulo;
	}

	public void setFrmRectangulo(JFrame frmRectangulo) {
		this.frmRectangulo = frmRectangulo;
	}

	private JFrame frmRectangulo;
	private JTextField base;
	private JTextField altura;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Rectangulo window = new Rectangulo();
					window.frmRectangulo.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Rectangulo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRectangulo = new JFrame();
		frmRectangulo.setResizable(false);
		frmRectangulo.setTitle("Rectangulo");
		frmRectangulo.setBounds(100, 100, 350, 200);
		frmRectangulo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRectangulo.getContentPane().setLayout(null);
		
		JLabel lblDimeBase = new JLabel("Dime base");
		lblDimeBase.setBounds(23, 31, 65, 14);
		frmRectangulo.getContentPane().add(lblDimeBase);
		
		JLabel lblDimeAltura = new JLabel("Dime altura");
		lblDimeAltura.setBounds(23, 70, 65, 14);
		frmRectangulo.getContentPane().add(lblDimeAltura);
		
		base = new JTextField();
		base.setBounds(88, 28, 40, 20);
		frmRectangulo.getContentPane().add(base);
		base.setColumns(10);
		
		altura = new JTextField();
		altura.setBounds(88, 67, 40, 20);
		frmRectangulo.getContentPane().add(altura);
		altura.setColumns(10);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.setBounds(193, 31, 106, 53);
		frmRectangulo.getContentPane().add(btnCalcular);
		
		JButton Salir = new JButton("Salir");
		Salir.setBounds(209, 119, 89, 23);
		frmRectangulo.getContentPane().add(Salir);
	
		//Eventos
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double resultado = Double.parseDouble(base.getText()) * Double.parseDouble(altura.getText());
				JOptionPane.showMessageDialog(null, "El area es: "+Double.toString(resultado));
			}
		});
		Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmRectangulo.dispose();
			}
		});
	}
}
