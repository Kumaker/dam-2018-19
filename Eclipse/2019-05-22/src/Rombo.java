import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Rombo {

	public JFrame getFrmRombo() {
		return frmRombo;
	}

	public void setFrmRombo(JFrame frmRombo) {
		this.frmRombo = frmRombo;
	}

	private JFrame frmRombo;
	private JTextField dia1;
	private JTextField dia2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Rombo window = new Rombo();
					window.frmRombo.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Rombo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRombo = new JFrame();
		frmRombo.setTitle("Rombo");
		frmRombo.setResizable(false);
		frmRombo.setBounds(100, 100, 350, 200);
		frmRombo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRombo.getContentPane().setLayout(null);
		
		JLabel lblDimeUnaDiagonal = new JLabel("1\u00BA diagonal");
		lblDimeUnaDiagonal.setBounds(10, 43, 100, 14);
		frmRombo.getContentPane().add(lblDimeUnaDiagonal);
		
		JLabel lblDiagonal = new JLabel("2\u00BA diagonal");
		lblDiagonal.setBounds(10, 80, 100, 14);
		frmRombo.getContentPane().add(lblDiagonal);
		
		dia1 = new JTextField();
		dia1.setBounds(91, 40, 40, 20);
		frmRombo.getContentPane().add(dia1);
		dia1.setColumns(10);
		
		dia2 = new JTextField();
		dia2.setBounds(91, 77, 40, 20);
		frmRombo.getContentPane().add(dia2);
		dia2.setColumns(10);
		
		JButton Calcular = new JButton("Calcular");
		Calcular.setBounds(198, 39, 91, 55);
		frmRombo.getContentPane().add(Calcular);
		
		JButton Salir = new JButton("Salir");
		Salir.setBounds(200, 117, 89, 23);
		frmRombo.getContentPane().add(Salir);
		
		//Eventos
		Calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double resultado = (Double.parseDouble(dia1.getText()) * Double.parseDouble(dia2.getText()))/2;
				JOptionPane.showMessageDialog(null, "El area es: "+Double.toString(resultado));
			}
		});
		Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmRombo.dispose();
			}
		});
	}

}
