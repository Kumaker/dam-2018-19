import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Trapecio {

	public JFrame getFrmTrapecio() {
		return frmTrapecio;
	}

	public void setFrmTrapecio(JFrame frmTrapecio) {
		this.frmTrapecio = frmTrapecio;
	}

	private JFrame frmTrapecio;
	private JTextField bg;
	private JTextField bp;
	private JTextField al;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Trapecio window = new Trapecio();
					window.frmTrapecio.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Trapecio() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTrapecio = new JFrame();
		frmTrapecio.setResizable(false);
		frmTrapecio.setTitle("Trapecio");
		frmTrapecio.setBounds(100, 100, 350, 200);
		frmTrapecio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTrapecio.getContentPane().setLayout(null);
		
		JLabel lblBaseGrande = new JLabel("Base grande");
		lblBaseGrande.setBounds(10, 22, 84, 14);
		frmTrapecio.getContentPane().add(lblBaseGrande);
		
		JLabel lblBasePequea = new JLabel("Base peque\u00F1a");
		lblBasePequea.setBounds(10, 60, 84, 14);
		frmTrapecio.getContentPane().add(lblBasePequea);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(10, 100, 46, 14);
		frmTrapecio.getContentPane().add(lblAltura);
		
		bg = new JTextField();
		bg.setBounds(100, 19, 40, 20);
		frmTrapecio.getContentPane().add(bg);
		bg.setColumns(10);
		
		bp = new JTextField();
		bp.setColumns(10);
		bp.setBounds(100, 57, 40, 20);
		frmTrapecio.getContentPane().add(bp);
		
		al = new JTextField();
		al.setColumns(10);
		al.setBounds(100, 97, 40, 20);
		frmTrapecio.getContentPane().add(al);
		
		JButton Calcular = new JButton("Calcular");
		Calcular.setBounds(195, 18, 89, 56);
		frmTrapecio.getContentPane().add(Calcular);
		
		JButton Salir = new JButton("Salir");
		Salir.setBounds(195, 96, 89, 23);
		frmTrapecio.getContentPane().add(Salir);
	
		//Eventos
		Calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double resultado = ((Double.parseDouble(bg.getText()) + Double.parseDouble(bp.getText()))/2)*Double.parseDouble(al.getText());
				JOptionPane.showMessageDialog(null, "El area es: "+Double.toString(resultado));
			}
		});
		Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmTrapecio.dispose();
			}
		});
	}

}
