import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Triangulo {

	public JFrame getFrmTriangulo() {
		return frmTriangulo;
	}

	public void setFrmTriangulo(JFrame frmTriangulo) {
		this.frmTriangulo = frmTriangulo;
	}

	private JFrame frmTriangulo;
	private JTextField base;
	private JTextField altura;

	/**
	 * Launch the application.
	 */	public static void main(String[] args) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						Triangulo window = new Triangulo();
						window.frmTriangulo.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	/**
	 * Create the application.
	 */
	public Triangulo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTriangulo = new JFrame();
		frmTriangulo.setResizable(false);
		frmTriangulo.setTitle("Triangulo");
		frmTriangulo.setBounds(100, 100, 350, 200);
		frmTriangulo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTriangulo.getContentPane().setLayout(null);
		
		JLabel lblDimeLosLados = new JLabel("Dime los lados");
		lblDimeLosLados.setBounds(20, 72, 115, 14);
		frmTriangulo.getContentPane().add(lblDimeLosLados);
		
		base = new JTextField();
		base.setBounds(131, 41, 25, 20);
		frmTriangulo.getContentPane().add(base);
		base.setColumns(10);
		
		altura = new JTextField();
		altura.setBounds(131, 102, 25, 20);
		frmTriangulo.getContentPane().add(altura);
		altura.setColumns(10);
		
		JLabel lblBase = new JLabel("base");
		lblBase.setBounds(166, 43, 44, 17);
		frmTriangulo.getContentPane().add(lblBase);
		
		JLabel lblLado = new JLabel("Altura");
		lblLado.setBounds(166, 104, 44, 17);
		frmTriangulo.getContentPane().add(lblLado);
		
		JButton Calcular = new JButton("Calcular");
		Calcular.setBounds(220, 55, 95, 49);
		frmTriangulo.getContentPane().add(Calcular);
		
		JButton Salir = new JButton("Salir");
		Salir.setBounds(220, 115, 89, 23);
		frmTriangulo.getContentPane().add(Salir);
		
		//Eventos
		
		Calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double resultado = (Double.parseDouble(base.getText()) * Double.parseDouble(altura.getText()))/2;
				JOptionPane.showMessageDialog(null, "El area es: "+Double.toString(resultado));
			}
		});
		Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmTriangulo.dispose();
			}
		});
	}

}
