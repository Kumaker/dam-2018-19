import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class calculadora {
	private JFrame frmCalculadora;
	private JTextField resultado;
	private char op = ' ';

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					calculadora window = new calculadora();
					window.frmCalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public calculadora() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculadora = new JFrame();
		frmCalculadora.setResizable(false);
		frmCalculadora.setTitle("Calculadora");
		frmCalculadora.setBounds(100, 100, 300, 384);
		frmCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCalculadora.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		frmCalculadora.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		resultado = new JTextField();
		panel.add(resultado);
		resultado.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		frmCalculadora.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(4, 4, 0, 0));
		
		JButton siete = new JButton("7");
		siete.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(siete);
		
		JButton ocho = new JButton("8");
		ocho.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(ocho);
		
		JButton nueve = new JButton("9");
		nueve.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(nueve);
		
		JButton dividir = new JButton("/");
		dividir.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(dividir);
		
		JButton cuatro = new JButton("4");
		cuatro.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(cuatro);
		
		JButton cinco = new JButton("5");
		cinco.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(cinco);
		
		JButton seis = new JButton("6");
		seis.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(seis);
		
		JButton multiplicar = new JButton("X");
		multiplicar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(multiplicar);
		
		JButton uno = new JButton("1");
		uno.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(uno);
		
		JButton dos = new JButton("2");
		dos.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(dos);
		
		JButton tres = new JButton("3");
		tres.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(tres);
		
		JButton restar = new JButton("-");
		restar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(restar);
		
		JButton igual = new JButton("=");
		igual.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(igual);
		
		JButton cero = new JButton("0");
		cero.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(cero);
		
		JButton coma = new JButton(",");
		coma.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(coma);
		
		JButton sumar = new JButton("+");
		sumar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(sumar);
		
		JPanel panel_2 = new JPanel();
		frmCalculadora.getContentPane().add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new GridLayout(1, 0, 0, 0));
		
		JButton limpiar = new JButton("BORRAR");
		limpiar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_2.add(limpiar);
	



// Eventos
		siete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resultado.setText(resultado.getText()+"7");
			}
		});
		ocho.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"8");
			}
		});
		nueve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"9");
			}
		});
		dividir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"/");
				op = '/';
				sumar.setEnabled(false);
				restar.setEnabled(false);
				multiplicar.setEnabled(false);
				dividir.setEnabled(false);
			}
		});
		cuatro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"4");
			}
		});
		cinco.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"5");
			}
		});
		seis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"6");
			}
		});
		multiplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"X");
				op = 'X';
				sumar.setEnabled(false);
				restar.setEnabled(false);
				multiplicar.setEnabled(false);
				dividir.setEnabled(false);
			}
		});
		uno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"1");
			}
		});
		dos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"2");
			}
		});
		tres.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"3");
			}
		});
		restar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"-");
				op = '-';
				sumar.setEnabled(false);
				restar.setEnabled(false);
				multiplicar.setEnabled(false);
				dividir.setEnabled(false);
			}
		});
		igual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"=");
				switch (op) {
				case '+':
					int posicion = resultado.getText().indexOf('+');
					String num1 = resultado.getText().substring(0, posicion);
					String num2 = resultado.getText().substring(posicion+1, resultado.getText().length());
					double total = Double.parseDouble(num1) + Double.parseDouble(num2);
					resultado.setText(Double.toString(total));
					break;
				case '-':
					
					break;
				case 'X':
					
					break;
				case '/':
					
					break;
				}
			}
		});
		cero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"0");
			}
		});
		limpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText("");
				sumar.setEnabled(true);
				restar.setEnabled(true);
				multiplicar.setEnabled(true);
				dividir.setEnabled(true);
			}
		});
		sumar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"+");
				op = '+';
				sumar.setEnabled(false);
				restar.setEnabled(false);
				multiplicar.setEnabled(false);
				dividir.setEnabled(false);
			}
		});
		coma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+",");
			}
		});
	}
}