import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Hija {

	private JFrame Hija;

	public JFrame getFrame() {
		return Hija;
	}

	public void setFrame(JFrame frame) {
		this.Hija = frame;
	}
	
	Padre padre;
	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public Hija(Padre padre) {
		initialize();
		this.padre=padre;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Hija = new JFrame();
		Hija.setResizable(false);
		Hija.setTitle("Hija");
		Hija.setBounds(100, 100, 450, 300);
		Hija.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Hija.getContentPane().setLayout(null);
		Hija.setLocationRelativeTo(null);
		
		JButton btnIrAPadre = new JButton("Ir a padre");
		btnIrAPadre.setBounds(167, 120, 89, 23);
		Hija.getContentPane().add(btnIrAPadre);
		
		// Eventos
		
		btnIrAPadre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				padre.getFrame().setVisible(true);
				Hija.dispose();
			}
		});
	}
}
