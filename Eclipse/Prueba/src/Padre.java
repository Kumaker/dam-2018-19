import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Padre {

	private JFrame Padre;

	public JFrame getFrame() {
		return Padre;
	}

	public void setFrame(JFrame frame) {
		this.Padre = frame;
	}

	Hija hija = new Hija(this);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Padre window = new Padre();
					window.Padre.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Padre() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Padre = new JFrame();
		Padre.setResizable(false);
		Padre.setTitle("FrmPadre");
		Padre.setBounds(100, 100, 450, 300);
		Padre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Padre.getContentPane().setLayout(null);
		Padre.setLocationRelativeTo(null);
		
		JButton btnHija = new JButton("Ir a hija");
		btnHija.setBounds(172, 120, 89, 23);
		Padre.getContentPane().add(btnHija);
		
		// Eventos
		
		btnHija.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hija.getFrame().setVisible(true);
				Padre.dispose();
				JOptionPane.showMessageDialog(Padre, "Ha cambiado de pagina", "Informacion", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
	}

}
