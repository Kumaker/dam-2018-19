function crearFigura(){
	var figura=document.getElementById('figura').value; /*Nombre de la figura seleccionada*/
	var fila=parseInt(document.getElementById('fila').value); /*Fila en la que hemos colocado la figura*/
	var columna=parseInt(document.getElementById('columna').value); /*columna en la que hemos colocado la figura*/ /*Al principio como no habia hecho un parse int se concatenaba con el + en vez de sumarlo porque lo cogia como un string ej= casilla_00+1 = casilla_001 y nosotros queremos casilla_01*/
	var casilla=document.getElementById('casilla_'+fila+''+columna); /*nombre de la casilla juntando casilla_+fila+columna (casilla_01)*/
	var casillas_mov=movimiento(fila,columna,figura);
	pintaCasillas(casillas_mov,fila,columna); /*Las Casillas que queremos pintar y el valor va cambiando todo el rato por los for*/
	casilla.innerHTML=figura;

	switch(figura){
		case'Peon': /*Falta meter el tamaño al unicode con .style.height¿?*/
			casilla.innerHTML="&#9817;";
		break;
		case 'Alfil':
			casilla.innerHTML="&#9815;";
		break;
		case 'Rey':
			casilla.innerHTML="&#9812;";
		break;
		case 'Reina':
			casilla.innerHTML="&#9813;";
		break;
		case 'Torre':
			casilla.innerHTML="&#9814;";
		break;
	}

}

function movimiento(fila,columna,figura){ //La funcion "movimiento" tiene los valores fila(el numero que hayamos escogido) columna(el numero que hayamos escogido) figura(El nombre de la figura que hayamos escogido para los case del switch)
	var casillas_mod; //Creamos casillas_mod que va a ser una variable que afectara a todos los casos ya que sera el array en el que almacenaremos todas las celdas que hay que colorear
	switch(figura){
		case 'Peon': //De la figura "peon" solo tiene 3 movimientos "hacia delante, obliquo izquierda y oblicuo derecha"
			if (fila-1 >=0 && columna >=0)
			var mov_1=['casilla_'+(fila-1)+''+columna]; // Movimiento 1 --> Arriba
			if (fila-1 >=0 && columna+1 <=7)
			var mov_2=['casilla_'+(fila-1)+''+(columna+1)]; //Movimiento 2 --> Oblicuo derecha
			if (fila-1 >=0 && columna-1 >=0)
			var mov_3=['casilla_'+(fila-1)+''+(columna-1)]; //Movimiento 3 --> Oblicuo izquierda
			var mov_fin=mov_1; //igualamos mov_1 a mov_fin porque es tonteria hacer un if para mov_1 sabiendo que mov_fin esta vacio, así que directamente lo rellenamos con mov_1 (si mov_1 tiene algún valor)
				if (mov_fin!=""){
					mov_fin+=','+mov_2; //Concatenamos todos los datos que vamos sacando separados por comas para que luego tengamos un simbolo con el que cortar el string
				}else{
					mov_fin=mov_2;
				}
				if (mov_fin!=""){
					mov_fin+=','+mov_3;
				}else{
					mov_fin=mov_3;
				}
			casillas_mod=mov_fin.split(','); //Hacemos que split corte el string por comas y en cada celda del array guarda 1 valor por cada corte.
		break;
		case 'Alfil':
			//Movimiento 1 --> Oblicuo abajo derecha
			var mov_1=""
			for (var f = fila, c = columna; f <= 7 && c<=7; f++,c++) {
				if(mov_1==""){
					mov_1+='casilla_'+f+''+c;   //En el primer if no hay coma porque si tiene que meter un solo valor, entorpece. En el else si, porque necesitamos un simbolo para que luego el split tenga de donde cortar.
				}else{
					mov_1+=',casilla_'+f+''+c;
				}
			}
			//Movimiento 2 --> Oblicuo abajo izquierda
			var mov_2=""
			for (var f = fila, c = columna; f <= 7 && c>=0; f++,c--) {
				if(mov_2==""){
					mov_2+='casilla_'+f+''+c;   //split lo que nos permite es cortar el array en trozos diciendole por donde queremos que corte.
				}else{
					mov_2+=',casilla_'+f+''+c;
				}
			}
			//Movimiento 3 --> Oblicuo arriba derecha
			var mov_3=""
			for (var f = fila, c = columna; f >= 0 && c<=7; f--,c++) {
				if(mov_3==""){
					mov_3+='casilla_'+f+''+c;   //he necesitado usar split porque no encontraba una manera en js de darle un tamaño dinamico a un array.
				}else{
					mov_3+=',casilla_'+f+''+c;
				}
			}
			//Movimiento 4 --> Oblicuo arriba izquierda
			var mov_4=""
			for (var f = fila, c = columna; f >= 0 && c>=0; f--,c--) {
				if(mov_4==""){
					mov_4+='casilla_'+f+''+c;   //.
				}else{
					mov_4+=',casilla_'+f+''+c;
				}
			}
			var mov_fin=mov_1;
			if(mov_2!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_2;
				}else{
					mov_fin=mov_2;
				}
			}
			if(mov_3!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_3;
				}else{
					mov_fin=mov_3;
				}
			}
			if(mov_4!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_4;
				}else{
					mov_fin=mov_4;
				}
			}
			casillas_mod=mov_fin.split(',');
		break;
		case 'Rey':
		    	if (fila-1 >=0 && columna >=0)
		        	var mov_1=['casilla_'+(fila-1)+''+columna]; //Movimiento 1 --> Arriba
		        if (fila-1 >=0 && columna+1 <=7)
		        	var mov_2=['casilla_'+(fila-1)+''+(columna+1)]; //Movimiento 2 --> Oblicuo arriba derecha
		        if (fila-1 >=0 && columna-1 >=0)
		     	    var mov_3=['casilla_'+(fila-1)+''+(columna-1)]; //Movmiento 3 --> Oblicuo arriba izquierda
		     	if (fila >=0 && columna+1 <=7)
		    	    var mov_4=['casilla_'+(fila)+''+(columna+1)]; //Movimiento 4 --> Derecha
		    	if (fila >=0 && columna-1 >=0)
		        	var mov_5=['casilla_'+(fila)+''+(columna-1)]; //Movimiento 5 --> Izquierda
		        if (fila+1 <=7 && columna >=0)
		        	var mov_6=['casilla_'+(fila+1)+''+(columna)]; //Movimiento 6 --> Abajo
		        if (fila+1 <=7 && columna-1 >=0)
		        	var mov_7=['casilla_'+(fila+1)+''+(columna-1)]; //Movimiento 7 --> Oblicuo abajo izquierda
		        if (fila+1 <=7 && columna+1 <=7)
		        	var mov_8=['casilla_'+(fila+1)+''+(columna+1)]; //Movimiento 8 --> Oblicuo abajo derecha
		        	var mov_fin=mov_1;
				if (mov_fin!=""){
					mov_fin+=','+mov_2;
				}else{
					mov_fin=mov_2;
				}
				if (mov_fin!=""){
					mov_fin+=','+mov_3;
				}else{
					mov_fin=mov_3;
				}
				if (mov_fin!=""){
					mov_fin+=','+mov_4;
				}else{
					mov_fin=mov_4;
				}
				if (mov_fin!=""){
					mov_fin+=','+mov_5;
				}else{
					mov_fin=mov_5;
				}
				if (mov_fin!=""){
					mov_fin+=','+mov_6;
				}else{
					mov_fin=mov_6;
				}
				if (mov_fin!=""){
					mov_fin+=','+mov_7;
				}else{
					mov_fin=mov_7;
				}
				if (mov_fin!=""){
					mov_fin+=','+mov_8;
				}else{
					mov_fin=mov_8;
				}
			casillas_mod=mov_fin.split(',');
		break;
		case 'Reina':
		//Movimiento 1 --> Oblicuo abajo derecha
		var mov_1=""
			for (var f = fila, c = columna; f <= 7 && c<=7; f++,c++) {
				if(mov_1==""){
					mov_1+='casilla_'+f+''+c;   //En el primer if no hay coma porque si tiene que meter un solo valor, entorpece. En el else si, porque necesitamos partir algo para luego el split.
				}else{
					mov_1+=',casilla_'+f+''+c;
				}
			}
			//Movimiento 2 --> Oblicuo abajo izquierda
			var mov_2=""
			for (var f = fila, c = columna; f <= 7 && c>=0; f++,c--) {
				if(mov_2==""){
					mov_2+='casilla_'+f+''+c;   //split lo que nos permite es cortar el array en trozos diciendole por donde queremos que corte.
				}else{
					mov_2+=',casilla_'+f+''+c;
				}
			}
			//Movimiento 3 --> Oblicuo arriba derecha
			var mov_3=""
			for (var f = fila, c = columna; f >= 0 && c<=7; f--,c++) {
				if(mov_3==""){
					mov_3+='casilla_'+f+''+c;   //he necesitado usar split porque no encontraba una manera en js de darle un tamaño dinamico a un array.
				}else{
					mov_3+=',casilla_'+f+''+c;
				}
			}
			//Movimiento 4 --> Oblicuo arriba izquierda
			var mov_4=""
			for (var f = fila, c = columna; f >= 0 && c>=0; f--,c--) {
				if(mov_4==""){
					mov_4+='casilla_'+f+''+c;   //.
				}else{
					mov_4+=',casilla_'+f+''+c;
				}
			//Movimiento 5 --> Arriba
			}
			var mov_5="";
			for (var f = fila; f <= 7; f++) {
				if(mov_5==""){
					mov_5+='casilla_'+f+''+columna;   
				}else{
					mov_5+=',casilla_'+f+''+columna;
				}
			}
			//Movimiento 6 --> Abajo
			var mov_6="";
			for (var f = fila; f >= 0; f--) {
				if(mov_6==""){
					mov_6+='casilla_'+f+''+columna;
				}else{
					mov_6+=',casilla_'+f+''+columna;
				}
			}
			//Movimiento 7 --> Derecha
			var mov_7="";
			for (var c = columna; c <= 7; c++) {
				if(mov_7==""){
					mov_7+='casilla_'+fila+''+c;
				}else{
					mov_7+=',casilla_'+fila+''+c;
				}
			}
			//Movimiento 8 --> Izquierda
			var mov_8="";
			for (var c = columna; c >= 0; c--) {
				if(mov_8==""){
					mov_8+='casilla_'+fila+''+c;
				}else{
					mov_8+=',casilla_'+fila+''+c;
				}
			}
			var mov_fin=mov_1;
			if(mov_2!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_2;
				}else{
					mov_fin=mov_2;
				}
			}
			if(mov_3!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_3;
				}else{
					mov_fin=mov_3;
				}
			}
			if(mov_4!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_4;
				}else{
					mov_fin=mov_4;
				}
			}
			if(mov_5!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_5;
				}else{
					mov_fin=mov_5;
				}
			}
			if(mov_6!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_6;
				}else{
					mov_fin=mov_6;
				}
			}
			if(mov_7!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_7;
				}else{
					mov_fin=mov_7;
				}
			}
			if(mov_8!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_8;
				}else{
					mov_fin=mov_8;
				}
			}
			casillas_mod=mov_fin.split(',');
		break;
		case 'Torre':
		//Movimiento 1 --> Abajo
			var mov_1="";
			for (var f = fila; f <= 7; f++) {
				if(mov_1==""){
					mov_1+='casilla_'+f+''+columna;   
				}else{
					mov_1+=',casilla_'+f+''+columna;
				}
			}
		//Movimiento 2 --> Arriba
			var mov_2="";
			for (var f = fila; f >= 0; f--) {
				if(mov_2==""){
					mov_2+='casilla_'+f+''+columna;
				}else{
					mov_2+=',casilla_'+f+''+columna;
				}
			}
		//Movimiento 3 --> Derecha
			var mov_3="";
			for (var c = columna; c <= 7; c++) {
				if(mov_3==""){
					mov_3+='casilla_'+fila+''+c;
				}else{
					mov_3+=',casilla_'+fila+''+c;
				}
			}
		//Movimiento 4 --> Izquierda
			var mov_4="";
			for (var c = columna; c >= 0; c--) {
				if(mov_4==""){
					mov_4+='casilla_'+fila+''+c;
				}else{
					mov_4+=',casilla_'+fila+''+c;
				}
			}
			var mov_fin=mov_1;
			if(mov_2!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_2;
				}else{
					mov_fin=mov_2;
				}
			}
			if(mov_3!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_3;
				}else{
					mov_fin=mov_3;
				}
			}
			if(mov_4!=""){
				if(mov_fin!=""){
					mov_fin+=','+mov_4;
				}else{
					mov_fin=mov_4;
				}
			}
			casillas_mod=mov_fin.split(',');
		break;
	}
	return casillas_mod;
}

function pintaCasillas(casillas_mod,fila,columna){ //La Funcion pintaCasillas es la que va a añadir a todas las casillas que nos pase el array "casillas_mod" un "#" indicando el posible movimiento de las figuras
	for (var i = casillas_mod.length - 1; i >= 0; i--) {
		if(casillas_mod[i]!=='undefined' && casillas_mod[i]!=='casilla_'+fila+''+columna){ 
			document.getElementById(casillas_mod[i]).style.color='red'; //Hacemos que el color con el que se escriba en HTML sea el rojo
			document.getElementById(casillas_mod[i]).innerHTML='#'; //Hacemos que lo que se pinte sea un "#"
		}
	}
} /*Hay que meter la funcion pintacasillas en un IF para que, si da una valor negativo no entre al bucle si no, cuando entra al bucle con un valor negativo, se sale y no pinta el resto.*/