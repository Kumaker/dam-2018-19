function CalcularMayor()
{
	var n1Cadena,
	n1Entero,
	n2Cadena,
	n2Entero,
	resultado;

	n1Cadena = document.getElementById('numero1').value;  //Para que solo me coja el valor y no toda la caja del imput
	n1Entero = parseInt(n1Cadena);
	n2Cadena = document.getElementById('numero2').value;
	n2Entero = parseInt(n2Cadena);

	resultado = document.getElementById('resultado');
	if (n1Entero > n2Entero)
		resultado.innerHTML +=
			"<br> El mayor es " + n1Entero;
	else
		resultado.innerHTML +=
			"<br> El mayor es " + n2Entero;
}