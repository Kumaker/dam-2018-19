function CalcularDiasMes()
{
	var mesCadena,
	mes,
	diasMes,
	resultado;

	mesCadena = document.getElementById('numeroMes').value;
	mes = parseInt(mesCadena);

	switch(mes)
	{/*
		case 1: diasMes = 31;
			break;
		case 2: diasMes = 28;
			break;
		case 3: diasMes = 31;
			break;
		case 4: diasMes = 30;
			break;
		case 5: diasMes = 31;
			break;
		case 6: diasMes = 30;
			break;
		case 7: diasMes = 31;
			break;
		case 8: diasMes = 31;
			break;
		case 9: diasMes = 30;
			break;
		case 10: diasMes = 31;
			break;
		case 11: diasMes = 30;
			break;
		case 12: diasMes = 31;
			break;
		default:diasMes = 99;
			break
			*/

		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12: diasMes = 31;
			break;
		case 4:
		case 6:
		case 9:
		case 11: diasMes = 30;
			break;
		case 2: diasMes = 28
			break;
		default:diasMes = 99;
			break;
	}
	resultado = document.getElementById('resultado');
	
	if(diasMes!=99)
	resultado.innerHTML +=
		"El mes " + mes + " tiene " + diasMes + " dias.";
	else
		resultado.innerHTML +=
		"El mes " + mes + " no es correcto (prueba 1-12)";

}

function CalcularTabla()
{
	var nCadena,
	n,
	i,
	resultado;

	nCadena = document.getElementById('numeroTabla').value;
	n = parseInt(nCadena);

	resultado = document.getElementById('resultado');
    i=1;
	while (i < 11) {
		resultado.innerHTML +=
		"<br>" + "La Tabla es " + n + " x " + i + " = " + (n*i);
		i++;
	}
}

function CalcularTablaDW()
{
	var nCadena,
	n,
	i,
	resultado;

	nCadena = document.getElementById('numeroTabla').value;
	n = parseInt(nCadena);

	resultado = document.getElementById('resultado');
    i=0;
    do
    {
		resultado.innerHTML +=
		"<br>" + "La Tabla es " + n + " x " + i + " = " + (n*i);
		i++;
	}while (i <=10);
}

function CalcularTablaF()
{
	var nCadena,
	n,
	i,
	resultado;

	nCadena = document.getElementById('numeroTabla').value;
	n = parseInt(nCadena);

	resultado = document.getElementById('resultado');
    i=1;
	for(i=0; i<=10; i++) {
		resultado.innerHTML +=
		"<br>" + "La Tabla es " + n + " x " + i + " = " + (n*i);
	}
}

function EscribirDias()
{
	var TablaDias,
	i,
	resultado;

	TablaDias = new Array();
	TablaDias[0] = "Lunes";
	TablaDias[1] = "Martes";
	TablaDias[2] = "Miercoles";
	TablaDias[3] = "Jueves";
	TablaDias[4] = "Viernes";

	resultado = document.getElementById('resultado');
	//Recorrer la tablaDias con for
	for(i=0; i<5;i++)
	{
		resultado.innerHTML += "<br>" + TablaDias[i];
	}
	//Recorrer TablaDias con for IN
	for(i in TablaDias)
	{
		resultado.innerHTML += "<br>" + TablaDias[i];
	}
}