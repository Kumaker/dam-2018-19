function CalculaFechaMayorMenor()
{
	var d1,m1,a1,
		d2,m2,a2,
		resultado;

	d1 = parseInt(document.getElementById('dia1').value);
	m1 = parseInt(document.getElementById('mes1').value);
	a1 = parseInt(document.getElementById('ano1').value);
	d2 = parseInt(document.getElementById('dia2').value);
	m2 = parseInt(document.getElementById('mes2').value);
	a2 = parseInt(document.getElementById('ano2').value);
	resultado = document.getElementById('resultado');

	/* Comprobar que fecha 1 > fecha 2 */

	if ((a1 > a2) || (a1==a2) && (m1 > m2) || (a1==a2) && (m1==m2) && (d1 > d2))

		resultado.innerHTML = 
			"El mayor es " + d1 + "/" + m1 + "/" + a1;
	else
		resultado.innerHTML = 
			"El mayor es " + d2 + "/" + m2 + "/" + a2;
}