function CalculaEdad()
{
	var d1,m1,a1,
		d2,m2,a2,
		resultado;

	d1 = parseInt(document.getElementById('dia1').value);
	m1 = parseInt(document.getElementById('mes1').value);
	a1 = parseInt(document.getElementById('ano1').value);
	d2 = parseInt(document.getElementById('dia2').value);
	m2 = parseInt(document.getElementById('mes2').value);
	a2 = parseInt(document.getElementById('ano2').value);
	resultado = document.getElementById('resultado');

	/* Comprobar edad*/
	if (( m1 > m2) || (m1 == m2 && d1 > d2))
		resultado.innerHTML = " Tienes " + (a1 - a2) + " años";

	else
		resultado.innerHTML = " Tienes " + ((a1 - a2)-1) + " años";
}