function calcular() {
	
	var n1,
		n2,
		n3,
		mayor,
		menor,
		resultado;

	n1 = parseInt(document.getElementById('numero1').value);
	n2 = parseInt(document.getElementById('numero2').value);
	n3 = parseInt(document.getElementById('numero3').value);

	resultado = document.getElementById('resultado');

	if ((n1>n2) && (n1>n3))
		mayor = n1;
	else if (n2>n3)
		mayor = n2;
	else
		mayor = n3;

	if ((n1<n2) && (n1<n3))
		menor = n1;
	else if (n2<n3)
		menor = n2;
	else 
		menor = n3;

	resultado.innerHTML +=
	"<br> el numero mayor es " +mayor+ " y el numero menor es " +menor;
}