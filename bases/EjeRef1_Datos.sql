INSERT INTO CLIENTES VALUES (
('12345678A','Perico','de los Palotes','123456789','perico@todo.com'),
('11111111Z','Fulanito','de Tal','666666999','ful@nito.com')
);

INSERT INTO TIENDAS VALUES (
('Gran Via'.'Madrid','Madrid','C/Gran Vía 32','141414142','Lunes','Viernes',9,18),
('Castellana'.'Madrid','Madrid','C/Castellana 108','432112342','Lunes','Sabado',8,15)
);

INSERT INTO OPERADORAS VALUES(
('Vodafone','Rojo',99,540,'www.vodafone.com'),
('Movistar','Azul',98,610,'www.movistar.com')
);

INSERT INTO TARIFAS VALUES(
('Koala','Orange','4','GB','200','Si'),
('Ardilla','Vodafone','3','GB','300','Si')
);
