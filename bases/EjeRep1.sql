DROP DATABASE IF EXISTS METEO;
CREATE DATABASE METEO;
USE METEO;

CREATE TABLE CIUDAD(
codigo CHAR (5),
nombre VARCHAR(20),
CONSTRAINT PK_CIUDAD PRIMARY KEY (codigo)
);

CREATE TABLE TEMPERATURA(
dia DATE,
codigo_CIUDAD CHAR(5),
grados TINYINT UNSIGNED,
CONSTRAINT PK_TEMPERATURA PRIMARY KEY (grados),
CONSTRAINT FK_TEMPERATURA_CIUDAD FOREIGN KEY (codigo_CIUDAD) REFERENCES CIUDAD (codigo)
);

CREATE TABLE HUMEDAD(
dia DATE,
codigo_CIUDAD CHAR(5),
porcentaje TINYINT UNSIGNED COMMENT '%',
CONSTRAINT PK_HUMEDAD PRIMARY KEY (porcentaje),
CONSTRAINT FK_HUMEDAD_CIUDAD FOREIGN KEY (codigo_CIUDAD) REFERENCES CIUDAD (codigo)
);

CREATE TABLE INFORME(
ID INT AUTO_INCREMENT,
dia DATE,
codigo_CIUDAD CHAR(5),
CONSTRAINT PK_INFORME PRIMARY KEY (ID),
CONSTRAINT FK_INFORME_CIUDAD FOREIGN KEY (codigo_CIUDAD) REFERENCES CIUDAD (codigo)
);

