DROP DATABASE IF EXISTS Ejercicio2_Refuerzo;
CREATE DATABASE Ejercicio2_Refuerzo;

USE Ejercicio2_Refuerzo;

CREATE TABLE ACTORES (Codigo int(11) AUTO_INCREMENT,Nombre varchar(20) ,Fecha date ,NACIONALIDAD varchar(20) ,PRIMARY KEY (Codigo));

CREATE TABLE NAVES (Codigo int(11) AUTO_INCREMENT,NºTripulantes int(11) ,Nombre varchar(20) ,PRIMARY KEY (Codigo));

CREATE TABLE PELICULAS (Codigo int(11) AUTO_INCREMENT,Titulo varchar(20) ,Director varchar(40) ,Año year(4) ,PRIMARY KEY (Codigo));

CREATE TABLE PLANETAS (Codigo int(11)  AUTO_INCREMENT, Galaxia varchar(20), Nombre varchar(20),PRIMARY KEY (Codigo));

CREATE TABLE PERSONAJES_PELICULAS (Codigo_PERSONAJES int(11),Codigo_PELICULAS int(11) ,PRIMARY KEY (Codigo_PERSONAJES,Codigo_PELICULAS),FOREIGN KEY (Codigo_PERSONAJES) REFERENCES PELICULAS (Codigo));

CREATE TABLE PERSONAJES (Codigo int(11) AUTO_INCREMENT, Nombre varchar(20), Raza varchar(20), GRADO smallint(6), Codigo_ACTORES int(11), CodigoSuperior_PERSONAJES int(11),PRIMARY KEY (Codigo, CodigoSuperior_PERSONAJES), FOREIGN KEY (Codigo_ACTORES) REFERENCES  ACTORES (Codigo));



CREATE TABLE VISITAS (Codigo_NAVES int(11), Codigo_PLANETAS int(11), Codigo_PELICULAS int(11), FOREIGN KEY (Codigo_NAVES) REFERENCES  NAVES (Codigo),FOREIGN KEY (Codigo_PLANETAS) REFERENCES  PLANETAS (Codigo), FOREIGN KEY (Codigo_PELICULAS) REFERENCES  PELICULAS (Codigo));












