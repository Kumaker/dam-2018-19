INSERT INTO TRABAJADOR (ID, nombre, campo, nivel) VALUES (060,'Kelvin Sito','Electromecanica',5),
							(741,'Ramon','Cristales',4),
							(157,'Tablo Marmol','Electricidad',3),
							(158,'Pedro Picapiedra','Mecanica',2),
							(666,'Antonov','Chapa y pintura',1);

INSERT INTO DEPOSITO (nombre, direccion, prioridad, fecha_entrada, fecha_salida ) VALUES ('All Born Oz','C/Huanuco, 7',('Alta'),'2018-09-09','2018-09-25'),
											('All Born Oz','C/Cantarranas, 19',('Media,Alta'),'2018-09-16','2018-09-22'),
										       	('Deposito KK','C/Huanuco, 7',('Media'),'2018-10-01','2018-10-06'),
											('Deposito KK','C/Cantarranas, 19',('Media,Baja'),'2018-10-20','2018-10-28'),
											('Depositos Mr.Satan','C/Shenron, 1',('Baja'),'2018-09-09','2018-09-11');

INSERT INTO VEHICULO(Nº_bastidor, marca, combustible, ID_TRABAJADOR) VALUES ('12PX567PVW011415Q','Renault','Gasolina',666),
											('778JDK66JAVA8234Ñ','Citroen','Diesel',060),
											('558CSS759566KAPPA','Toyota','Electrico',158),
											('9862586ESPN787447','Aprilia','Diesel',666),
											('48XD54XX3559PLS66','Dacia','Gasolina',741),
											('4521HTML6526CSS55','Peugeot','Gasolina',157),
											('77451JHF7854DA148','Bultaco','Diesel',060),
											('17851236FM4846I59','Harley','Gasolina',666),
											('47412347LMDR48KJ8','Chopper','Diesel',158),
											('3654287KLD7852JD9','Ducati','Diesel',060);

INSERT INTO CLIENTE (nombre, telefono, apellido1, apellido2) VALUES ('Juan','678559847','Car','Lost'),
								('Dolores','654339854','Fuertes','De Barriga'),
								('Rosa','687445261','Mel','Troso'),
								('Debora','666533212','Mel','Troso'),
								('Tony','698447525','Stark','Loto');


INSERT INTO TRAE (Nº_bastidor_VEHICULO, fecha) VALUES ('12PX567PVW011415Q','2018-09-09'),
									('778JDK66JAVA8234Ñ','2018-09-09'),
									('558CSS759566KAPPA','2018-09-16'),
									('9862586ESPN787447','2018-09-16'),
									('48XD54XX3559PLS66','2018-09-18');

INSERT INTO COCHE (modelocoche, Nº_bastidor_VEHICULO) VALUES('Laguna','12PX567PVW011415Q'),
							('ZX','778JDK66JAVA8234Ñ'),
							('Prius','558CSS759566KAPPA'),
							('206','4521HTML6526CSS55'),
							('Sandero','48XD54XX3559PLS66');


INSERT INTO MOTO (modelomoto, Nº_bastidor_VEHICULO) VALUES('Albero','77451JHF7854DA148'),
						('Low Rider S','17851236FM4846I59'),
						('Shiver 900','9862586ESPN787447'),
						('Bear Bones','47412347LMDR48KJ8'),
						('Panigale','3654287KLD7852JD9');

INSERT INTO GARAJE (Nº_garaje, capacidad, plaza) VALUES (1,240,143),
							(3,100,071),
							(2,125,055),
							(4,175,086),
							(5,60,030);

INSERT INTO AVERIA (estado, fecha, grado, Nº_bastidor_VEHICULO) VALUES ('En proceso','2018-09-09','Leve','12PX567PVW011415Q'),
								('Pendiente','2018-09-09','Grave','778JDK66JAVA8234N'),
								('Reparado','2018-09-16','Medio','558CSS759566KAPPA'),
								('En proceso,Reparado','2018-09-16','Leve','9862586ESPN787447'),
								('En proceso','2018-10-01','Medio','4521HTML6526CSS55');

INSERT INTO ALMACEN (Cantidad_de_piezas, ID) VALUES(125,1),
						(10,2),
						(100,3),
						(22,4),
						(360,5);


INSERT INTO PIEZA (Precio, Tipo, Nº_referencia, ID_ALMACEN) VALUES(200,'Luna','1011010',2),
								(40,'Faros','1100110',1),
								(720,'Motor','1010101',3),
								(80,'Rueda','1110111',5),
								(60,'Chasis','1110001',4);

INSERT INTO SOLUCIONA(Nº_referencia_PIEZA) VALUES ('1110111'),
						('1010101'),
						('1110001'),
						('1011010'),
						('1110001');

INSERT INTO PROVEEDOR (Nombre, ID) VALUES ('Todo Cristales',1),
					('Electricidad Manolo e hijos',2),
					('La Chapa',3),
					('El Ferretero Feliz',4),
					('Master Herramientas',5);


INSERT INTO SUMINISTRA (Nº_referencia_PIEZA, cantidad, ID_PROVEEDOR) VALUES ('1011010',7,1),
									('1100110',100,2),
									('1010101',4,3),
									('1110001',15,4),
									('1110111',247,5);
