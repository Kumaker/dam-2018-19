/*Añadir auto increment en foreign key*/
ALTER TABLE PERSONAJES DROP FOREIGN KEY FK_Codigo_ACTORES;
ALTER TABLE ACTORES MODIFY Codigo INT AUTO_INCREMENT;
ALTER TABLE PERSONAJES ADD CONSTRAINT FK_Codigo_ACTORES FOREIGN KEY(Codigo_ACTORES) REFERENCES ACTORES(Codigo);

/*Añadir dato YEAR en vez de DATE*/
ALTER TABLE PELICULAS MODIFY Año YEAR;

/*Aceptar acentos*/
CHARSET utf8;

/*Actualizar en cascada foreign key*/
ALTER TABLE PERSONAJES DROP FOREIGN KEY FK_Codigo_ACTORES;
ALTER TABLE PERSONAJES ADD CONSTRAINT FK_Codigo_ACTORES FOREIGN KEY(Codigo_ACTORES) REFERENCES ACTORES(Codigo) ON UPDATE CASCADE;

/*Actualizar en cascada y que al borrar se quede con NULL*/
ALTER TABLE PERSONAJES DROP FOREIGN KEY FK_Codigo_ACTORES;
ALTER TABLE PERSONAJES ADD CONSTRAINT FK_Codigo_ACTORES FOREIGN KEY(Codigo_ACTORES) REFERENCES ACTORES(Codigo) ON UPDATE CASCADE ON DELETE SET NULL;

/*Eliminar campo CodigoSuperior*/
ALTER TABLE PERSONAJES DROP FOREIGN KEY FK_CodigoSuperior;
ALTER TABLE PESONAJES DROP COLUMN CodigoSuperior_PERSONAJES;

/*Añadir LadoFuerza en la tabla PERSONAJES*/
ALTER TABLE PERSONAJES ADD COLUMN LadoFuerza ENUM('Jedi', 'Sith');

/*Datos*/
INSERT INTO ACTORES (Nombre,Fecha,Nacionalidad) VALUES ('ARTURO','2018-02-02','Española');

/*Actualizar algun valor de la tabla no clave*/
UPDATE ACTORES SET Nombre='Arturo Sierra' WHERE Nombre='ARTURO';

/*Actualizar algun valor de la tabla clave*/
UPDATE ACTORES SET Codigo='5' WHERE Codigo='1';

/*Borrar algun valor de la tabla no clave*/
DELETE FROM ACTORES WHERE Nombre='Arturo Sierra';

/*Borrar algun valor de la tabla clave*/
DELETE FROM ACTORES WHERE Codigo='5';

/*Borrar tabla VISITAS*/
ALTER TABLE VISITAS DROP FOREIGN KEY FK_VISITAS_NAVES;
ALTER TABLE VISITAS DROP FOREIGN KEY FK_VISITAS_PLANETAS;
ALTER TABLE VISITAS DROP FOREIGN KEY FK_VISITAS_PELICULAS;
DROP TABLE VISITAS;

/*Crear view para piloto*/
CREATE DEFINER = 'piloto'@'localhost' VIEW ver_piloto AS SELECT * FROM aeropuerto_antigua.TRABAJADOR WHERE Puesto_de_trabajo = 'Piloto';

/*Borrar view*/
DROP VIEW IF EXISTS ver_piloto;

/*Ver fecha actual*/
SELECT CURDATE();

/*Desactivar y luego activar foreign key*/
SET foreign_key_checks=0;
SET foreign_key_checks=1;

/*Diferencia entre change y modify*/
