DROP PROCEDURE IF EXISTS holaProc;
DROP FUNCTION IF EXISTS holaFunc;

delimiter //

CREATE PROCEDURE holaProc (IN nombre VARCHAR(20))
BEGIN
SELECT (CONCAT('Hola ', nombre)) AS 'Salida';
END//

delimiter ;

CALL holaProc('Miguel');

CREATE FUNCTION holaFunc ()
RETURNS VARCHAR(30)
RETURN SELECT ('Hola');

SELECT holaFunc()
