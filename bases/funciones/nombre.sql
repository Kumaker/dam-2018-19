DROP PROCEDURE IF EXISTS nombre;

delimiter // 
CREATE PROCEDURE nombre(IN nombre VARCHAR(20))
BEGIN 
SELECT(CONCAT('hola', nombre)) AS 'Salida Terminal';
END//

delimiter ;

CALL nombre(Miguel);

