
#include <stdio.h> 
#include <stdlib.h>

#define ROJO 4
#define AMAR 2
#define AZUL 1

/* Función punto de entrada */
int main(){   
    char respuesta;
    int color = 0;

    printf("ves rojo (S/n)?: ");
    scanf(" %c", &respuesta);
        if(respuesta == 's')
        color |= ROJO;

    printf("ves amarillo (S/n)?: ");
    scanf(" %c", &respuesta);
        if(respuesta == 's')
        color |= AMAR;

    printf("ves azul (S/n)?: ");
    scanf(" %c", &respuesta);
        if(respuesta == 's')
        color |= AZUL;

    switch(color) { 
        case 0:
            printf("es negro\n");
            break;

        case 1:
            printf("es azul\n");
            break;

        case 2:
            printf("es amarillo\n");
            break;

        case 3:
            printf("es verde\n");
            break;

        case 4:
            printf("es rojo\n");
            break;

        case 5:
            printf("es morado\n");
            break;

        case 6:
            printf("es naranja\n");
            break;

        case 7:
            printf("es blanco\n");
            break;


    }

    return EXIT_SUCCESS;
}
