

#include <stdio.h> 
#include <stdlib.h>

#define ROJO 4
#define AMAR 2
#define AZUL 1

/* Función punto de entrada */
int main(){

    char respuesta;
    int color = 0;

    printf("ves rojo? (S/n): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= ROJO;                /*se puede usar cualquiera de las 2 (|= o +=) */

    printf("ves amarillo? (S/n): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color += AMAR;

    printf("ves azul? (S/n): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color += AZUL;

    switch(color) {
        case 0:
            printf("negro.\n");
            break;

        case 1:
            printf("azul.\n");
            break;

        case 2:
            printf("amarillo.\n");
            break;

        case 3:
            printf("verde.\n");
            break;

        case 4:
            printf("rojo.\n");
            break;

        case 5:
            printf("morado.\n");
            break;

        case 6:
            printf("naranja.\n");
            break;

        case 7:
            printf("blanco.\n");
            break;
    }
    return EXIT_SUCCESS;
}
