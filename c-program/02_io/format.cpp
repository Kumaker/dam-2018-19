
#include <stdio.h> 
#include <stdlib.h>

/* Función punto de entrada */
int main(){
   
    printf("%c", 0x61); /*para imprimir un caracter %c y despues el numero ascii del numero*/
    printf("%c", 97);  
    printf("%c", 'a'); /*todos muestran una "a"*/
    printf("%i", 97); /*%i nos imprime el numero que hemos puesto, %2i nos imprime con 2 caracteres*/
    printf("%lf/n", 3.2); /*%lf imprime numeros decimales, %6.2lf cuantos espacios antes del numero*/
    printf("\tholan\n"); /* \t  tabula*/ 
    
    int numero;
    printf("Num: ");
    scanf(" %i", &numero);
    printf("%i\n", numero);
    printf("Linea %i\n", 47);
    printf("Numero => [%p]: %i\n", &numero, numero); /*%p puntero*/

    int dia, annio;
    printf("Nacimiento: ");
    scanf(" %i/%*i/%i", &dia, &annio); /*caracter de supresión "*" */

    char hex[32];
    scanf(" %[0-9a-fA-F]", hex); /*Conjunto de seleccion*/
    scanf(" %[^0-9a-fA-F]", hex); /*conjunto de seleccion inverso*/


    return EXIT_SUCCESS;
}
