
#include <stdio.h> 
#include <stdlib.h>

/* Función punto de entrada */
int main(){

    int d, m, a; /*dias mes años de nacimiento*/
    int dias_vividos, date_d, date_m, date_a; /*dia de hoy, mes de hoy, año de hoy*/


    printf("Fecha de nacimiento: \n");
    scanf("%d/%d/%d", &d, &m, &a); /*%d es para que sepa que va a ser un dato decimal*/
    printf("¿Fecha de hoy?: \n");
    scanf("%d/%d/%d", &date_d, &date_m, &date_a);
    dias_vividos = (date_a - a)*365 + (date_m - m)*30 + (date_d - d);
    printf("has vivido %i dias\n", dias_vividos);


    return EXIT_SUCCESS;
}
