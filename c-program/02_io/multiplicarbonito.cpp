
// Autor Kumaker
// Fecha 25/01/2019

#include <stdio.h>
#include <stdlib.h>

#define VECES 11
#define MAX 0x100

void pon_titulo(int numero){ /*Parametro formal*/
	char titulo[MAX];

	sprintf(titulo,"toilet -fpagga --metal Tabla del %i", numero);
       	/*sprintf nos guarda lo que queramos imprimir en memoria */
	system(titulo); /*System estrae el dato de la memoria indicada*/


}
/* Función punto de entrada */
int main(){
	int numero;
        int resultado;
	//menu
	printf("que numero quieres multiplicar: ");
	scanf("%i", &numero);
	pon_titulo(numero);     //Llamada con parametro actual


	//Resultado
	for(int num=0; num<VECES; num++) {
		resultado= num * numero;
	    printf("%i*%i=%i\n", num, numero, resultado);
	}

	return EXIT_SUCCESS;
}
