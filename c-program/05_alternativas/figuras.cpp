
#include <stdio.h> 
#include <stdlib.h>

#define PI 3.14

/* Función punto de entrada */
int main(){
int menu = 0, ope1, ope2, res;
enum {
    cuadrado,
    triangulo,
    rectangulo,
    circulo,
    pentagono,
    FIGURAS
};

    printf("escoge la figura que quieres: \n 0 CUADRADO\n 1 TRIANGULO\n 2 RECTANGULO\n 3 CIRCULO\n 4 PENTAGONO\n introduzca el numero: ");
    scanf(" %i", &menu);

    switch(menu){
        case cuadrado:
            system ("toilet -fpagga CUADRADO");
            printf("dime el primer lado de tu cuadrado: \n");
            scanf(" %i", &ope1);
            printf(" ahora dime el segundo lado: \n");
            scanf(" %i", &ope2);
            res = ope1 * ope2;
            printf("esta es la base: %i \n", res);
            break;

        case triangulo:
            system ("toilet -fpagga TRIANGULO");
            printf("dime la base de tu triangulo: \n");
            scanf(" %i", &ope1);
            printf("ahora dime la altura: \n");
            scanf(" %i", &ope2);
            res = (ope1*ope2) /2;
            printf("esta es tu base: %i \n", res);
            break;

        case rectangulo:
            system ("toilet -fpagga RECTANGULO");
            printf("dime la base de tu rectangulo: \n");
            scanf(" %i", &ope1);
            printf("ahora dime la altura: \n");
            scanf(" %i", &ope2);
            res = ope1 * ope2;
            printf("esta es la base: %i \n", res);
            break;
 
        case circulo:
            system ("toilet -fpagga CIRCULO");
            printf("dime el primer radio de tu circulo: \n");
            scanf(" %i", &ope1);
            printf("ahora dime el segundo radio: \n");
            scanf(" %i", &ope2);
            res = PI * (ope1 * ope2);
            printf("esta es tu base: %i \n", res);
            break;

        case pentagono:
            system ("toilet -fpagga PENTAGONO");
            printf("dime el perimetro de tu pentagono: \n");
            scanf(" %i", &ope1);
            printf("ahora dime la apotema: \n");
            scanf(" %i", &ope2);
            res = (ope1 * ope2) /2;
            printf("esta es tu base: %i \n", res);
            break;
    }

    return EXIT_SUCCESS;
}
