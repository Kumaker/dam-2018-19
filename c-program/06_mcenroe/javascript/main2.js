var X = 0
var Y = 1
var radio = 200
var extra = 5

function rad(angulo){
    return angulo * Math.PI / 180
}
function main() {
    var centro = [500, 500]
    var lienzo = document.getElementById("lienzo").getContext("2d")
    lienzo.beginPath()
    lienzo.arc(centro[X], centro[Y], radio, 0, Math.PI * 2)
    lienzo.stroke()

    lienzo.beginPath() 
    for (var angulo=00; angulo<360; angulo+=360/10) {  
        lienzo.moveTo(centro[X], centro[Y])
        lienzo.lineTo(centro[X] + (radio+extra) * Math.cos( rad(angulo) ),
            centro[Y] + (radio+extra) * Math.sin( rad(angulo) ))
    }
    lienzo.stroke()
}
