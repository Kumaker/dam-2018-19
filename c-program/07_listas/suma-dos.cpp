
#include <stdio.h> 
#include <stdlib.h>

#define N 15

/* Función punto de entrada */
int main(){

    int emento[N];
/*Condiciones de entorno*/
    emento[1] = emento[0] = 1;

    /*Calculos*/
    for (int i =2; i<N; i++)
    emento[i] = emento[i-1] + emento[i-2];
    /*Salidas de datos*/
    for (int i=0; i<N; i++)
    printf("%i\n", emento[i]);

    for (int i=1; i<N; i++)
        printf(" %4lf\n", (double) emento[i] / emento[i-1]);
        return EXIT_SUCCESS;
}
