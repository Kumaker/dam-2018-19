
#include <stdio.h> 
#include <stdlib.h>

/* Función punto de entrada */
int main(){ 
  unsigned primo[] = {2 , 3, 5, 7, 11, 13, 17, 19, 23};
  unsigned elementos = (unsigned) sizeof(primo) / sizeof(int);
  unsigned *peeping = primo;
  char *tom     = (char *) primo;
  unsigned **police = &peeping;

  printf( "PRIMO:\n"
          "======\n"
          " Localización (%p)\n"
          " Elementos: %u [%u .. %u]\n"
          " Tamaño: %lu bytes.\n\n",
          primo,
          elementos,
          primo[0], primo[elementos-1],
          sizeof(primo));
 /*el "*" significa allí donde apunta
   *peeping se leería, "allá donde apunta peeping*/
  printf( "0: %u\n", peeping[0] );
  printf( "1: %u\n", peeping[1] );
  printf( "0: %u\n", peeping[0] );
  printf( "1: %u\n", peeping[1] );
  printf( "Tamaño %lu bytes.\n", sizeof(peeping) );
  printf( "\n" );

  /* Memory Demp - Volcado de memoria*/
  for (int i=0; i<sizeof(primo); i++)
  printf("%02X", *(tom + i));
  printf( "\n\n");

  printf( "police contiene %p\n", police);
  printf( "primo contiene %p\n", *police);
  printf( "primo[0] contine %u\n", **polict);

    return EXIT_SUCCESS;
}
