
#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada */
int main(){

    int l = 7;
    int menu = 0;

    printf(" escoge un numero para el dado del 1 al 6: ");
    scanf(" %i", &menu);

    switch(menu){
        case 1:
            for (int f=0; f<l; f++){
                for (int c=0; c<l; c++)
                    if(c==0 || f==0 || c==l-1 || f==l-1 || f==3 && c==3)
                        printf(" *");
                    else
                        printf("  ");
                printf("\n");
             }
            break;

        case 2:
            for (int f=0; f<l; f++){
                for (int c=0; c<l; c++)
                    if(c==0 || f==0 || c==l-1 || f==l-1 || f==4 && c==2 || f==2 && c==4)
                        printf(" *");
                    else
                        printf("  ");

                printf("\n");
            }
            break;

         case 3:
            for (int f=0; f<l; f++){
                for (int c=0; c<l; c++)
                    if(c==0 || f==0 || c==l-1 || f==l-1 || f==4 && c==2 || f==2 && c==4 || f==3 && c==3)
                        printf(" *");
                    else
                        printf("  ");

                printf("\n");
            }
            break;

         case 4:
            for (int f=0; f<l; f++){
                for (int c=0; c<l; c++)
                    if(c==0 || f==0 || c==l-1 || f==l-1 || f==2 && c==2 || f==4 && c==4 ||f==4 && c==2 || f==2 && c==4)
                        printf(" *");
                    else
                        printf("  ");

                printf("\n");
            }
            break;

         case 5:
            for (int f=0; f<l; f++){
                for (int c=0; c<l; c++)
                    if(c==0 || f==0 || c==l-1 || f==l-1 || f==2 && c==2||  f==2 && c==4|| f==3 && c==3|| f==4 && c==2 || f==4 && c==4)
                        printf(" *");
                    else
                        printf("  ");

                printf("\n");
            }
            break;

         case 6:
            for (int f=0; f<l; f++){
                for (int c=0; c<l; c++)
                    if(c==0 || f==0 || c==l-1 || f==l-1 || f==2 && c==2 || f==3 && c==2 ||f==4 && c==2 || f==2 && c==4 || f==3 && c==4 || f==4 && c==4)
                        printf(" *");
                    else
                        printf("  ");

                printf("\n");
            }
            break;
}
return EXIT_SUCCESS;
}
