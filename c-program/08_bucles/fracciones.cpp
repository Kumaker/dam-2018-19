
#include <stdio.h> 
#include <stdlib.h>

/* Función punto de entrada */
int main(){

    int numerador = 30,
        denominador=15;

    for (int pd=numerador/2; pd>1; pd--)
            if (numerador % pd == 0 && denominador % pd ==0){
                numerador /= pd;
                denominador /= pd;
            }
    printf ("%i / %i \n", numerador, denominador);

    return EXIT_SUCCESS;
}
