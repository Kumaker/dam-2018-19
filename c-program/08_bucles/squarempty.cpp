#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada */
int main(){

    int l = 5;

    for (int f=0; f<l; f++){
        for (int c=0; c<l; c++)
           if(c==0 || f==0 || c==l-1 || f==l-1 || f==c)
               printf(" *");
           else
               printf("  ");
    printf("\n");
}
    return EXIT_SUCCESS;
}
