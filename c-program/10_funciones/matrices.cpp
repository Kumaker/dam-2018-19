
#include <stdio.h> 
#include <stdlib.h>
#define N 3
#define D 3
/* Función punto de entrada */
int main(){

int res1, res2, res3, resf1, resf2;
double a[D][D] = {
        {1,3,2},
        {7,5,4},
        {4,8,2}
};
printf("la matriz que tenemos es: \n"
        "{1,3,2}\n"
        "{7,5,4}\n"
        "{4,8,2}\n");
double determinante = 0,
       determinante2 = 0,
       multiplicacion,
       resf;

for (int f=0; f<D; f++){
    multiplicacion = 1;
    for (int d=0; d<D; d++)
        multiplicacion *= a[(f+d)%D][0+d];
    determinante += multiplicacion;
}
for (int f=0; f<D; f++){
    multiplicacion = 1;
    for (int d=0; d<D; d++)
        multiplicacion *= a[(f+d)%D][2-d];
    determinante2 += multiplicacion;
}
resf= determinante - determinante2;
printf(" %.2lf\n", determinante);
printf(" %.2lf\n", determinante2);
printf(" %.2lf\n", resf);
/*
res1=(matriz[0][0] * matriz[1][1] * matriz[2][2]);
printf(" %i\n", res1);
res2=(matriz[1][0] * matriz[2][1] * matriz[0][2]);
printf(" %i\n", res2);
res3=(matriz[2][0] * matriz[0][1] * matriz[1][2]);
printf(" %i\n", res3);
resf1=(res1 + res2 + res3);
printf(" %i\n", resf1);

res1=(matriz[0][2] * matriz[1][1] * matriz[2][0]);
printf(" %i\n", res1);
res2=(matriz[1][2] * matriz[2][1] * matriz[0][0]);
printf(" %i\n", res2);
res3=(matriz[2][2] * matriz[0][1] * matriz[1][0]);
printf(" %i\n", res3);
resf2=(res1 + res2 + res3);
printf(" %i\n", resf2);
resf=(resf1 - resf2);
printf(" el resultado de la matriz= %i\n", resf);
*/
    return EXIT_SUCCESS;
}
