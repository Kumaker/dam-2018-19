
#include <stdio.h> 
#include <stdlib.h>
#define N 3
/* Función punto de entrada */
int main(){
    double determinante = 0,
           determinante2 = 0;

    double a[2][3]{
            {2,3,4},
            {1,3,2}
    };
    double b[3][1]{
            {1},
            {7},
            {2}
    };
    double c[2][1];

    for(int i=0; i<N; i++){
        determinante += a[0][i] * b[i][0];
    printf(" %.2lf\n", determinante);
    }
    for (int i=0; i<N; i++){
        determinante2 += a[1][i] * b[i][0];
    printf(" %.2lf\n", determinante2);
    }

/*    printf(" %.2lf\n",c[0][1]);
    printf(" %.2lf\n",c[1][1]);*/

    return EXIT_SUCCESS;
}
