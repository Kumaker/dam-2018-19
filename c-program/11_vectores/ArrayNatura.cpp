
#include <stdio.h> 
#include <stdlib.h>

#define N 10

/* Función punto de entrada */
int main(){

    int n[N] = {0,1,2,3,4,5,6,7,8,9};
    int resultado;
    
    for (int i=0; i<N; i++){
        resultado= n[i]*n[i];
        printf("La potencia de %i es %i\n",n[i],resultado);
    }

    return EXIT_SUCCESS;
}
