
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h>

#define MAX 0x100
#define INFORMA(...) if (verbose_flag)
{
    printf( __VA_ARGS__ );/*ES UNA MACRO*/
    printf("\n");
}
/* Función punto de entrada */
bool help_flag = false,
     verbose_flag = false;
const char *program_name;

void print_usage (FILE *pf, int exit_code) {
    fprintf (pf, "%s lo que sea.\n", program_name);
        exit (exit_code);
}

int divisores (int n, int list[MAX]){
    int pos = 0;

    for (int d=1; d<n; d++)
        if (n % d == 0)
            list[pos++] = d;

    return pos;
}

void imprime(int lista[MAX], int nceldas){
    for(int a=0; a<nceldas; a++)
        printf("%i", lista[a]);

    printf("\n");
}

int main(int argc, char *argv[]){

    int lista[MAX]
    int cuantos;

    int o;

    while ( (o = getopt(argc, argv, "hv")) != -1){
     switch (o){
        case 'h':
            help_flag = true;
            break;
        case 'v':
            verbose_flag = true;
            break;
        case '?':
            print_usage (stderr, 1);
            break;
     }
    }
    if (argc < 2)
        return EXIT_FAILURE;

    program_name = argv[0];

    if (help_flag)
        print_usage (stdout, 0);

    INFORMA ("Voy a coger el parametro de la terminal")
    int n = atoi (argv[optind]);
    INFORMA ("He recogido el parametro %i de la terminal",n)

    INFORMA ("Voy a calcular los divisores.")
    cuantos =  divisores (n, lista);
    INFORMA ("He encontrado %i", cuantos)

    imprime (lista, cuantos);

    return EXIT_SUCCESS;
}
