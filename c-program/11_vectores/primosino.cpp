
#include <stdio.h> 
#include <stdlib.h>

/* Función punto de entrada */
int main(){
    int d=0, numero;

    printf("¿que numero quieres?: ");
    scanf("%i", &numero); 

    for(int div=1; div<=numero; div++){
        if (numero % div == 0)
            d++;
    }
    if (d<=2)
        printf("es primo \n");
    else
        printf("no es primo \n");

    return EXIT_SUCCESS;

}

