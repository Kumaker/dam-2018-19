
#include <stdio.h> 
#include <stdlib.h>

void dimePrimo(int n){
    int d=0;
    for(int div=1; div<=n; div++){
        if (n % div == 0)
            d++;
    }
    if (d<=2)
        printf("es primo \n");
    else
        printf("no es primo \n");
}

/* Función punto de entrada */
int main(){
    int numero;

    printf("¿que numero quieres?: ");
    scanf("%i", &numero);
    dimePrimo(numero);
    return EXIT_SUCCESS;

}

