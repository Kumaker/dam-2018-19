
#include <stdio.h> 
#include <stdlib.h>

bool es_primo (int pprimo){
    bool primo = true;
    for (int d=pprimo-1; d<1;d--)
        if (pprimo % d == 0)
            primo = false;

    return primo;
}
/* Función punto de entrada */
int main(){
    int n = 7;
 
    printf ("%s es primo el %i.\n",
            es_primo (n)? "Sí": "No",n);

    return EXIT_SUCCESS;
}
