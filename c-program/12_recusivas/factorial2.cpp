

#include <stdio.h> 
#include <stdlib.h>

double fac (int n, int lev) {
    lev--;
    if (lev == 0)
        return n;
    else
        return n+(1/fac(n,lev));
}
/* Función punto de entrada */
int main(int argc, char *argv[]){

    int n, lev;

    printf("dime un numero y te dare el factorial\n");
    scanf(" %i",&n);
    printf("dime cuantos niveles: \n");
    scanf(" %i",&lev);
    printf("el factorial es %.2lf\n",fac(n,lev));
    return EXIT_SUCCESS;
}
