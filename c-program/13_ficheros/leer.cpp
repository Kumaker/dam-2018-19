#include <stdio.h>
#include <stdlib.h>

#define N 10
#define MAX 0x100

#define FILENAME "volvado.txt"
/* Función punto de entrada */
int main(int argc, char *argv[]){

    char nombre[N][MAX];
    FILE *pf;

    if ( !(pf = fopen (FILENAME, "r")) ){
        fprintf (stderr, "no encontré %s.\n", FILENAME);
        return EXIT_FAILURE;
    }

    /*Adquiere los datos desde el fichero*/
    for (int i=0; i<N; i++)
        fgets ( nombre[i], MAX, pf);

    /*Imprime los datos desde la memoria*/
    for (int i=0; i<N; i++)
        printf ("Fila %i: %s\n", i, nombre[i]);

    fclose (pf);

    return EXIT_SUCCESS;
}
