
#include <stdio.h> 
#include <stdlib.h>

#define FILENAME "volcado.txt"
/* Función punto de entrada */

int main(int argc, char *argv[]){
    FILE *pf;
    long int inicio, fin, distancia;

    if ( !(pf = fopen (FILENAME, "r")) ){
        fprintf (stderr, "Aín,\n");
    return EXIT_FAILURE;
}
    inicio = ftell (pf);
    fseek (pf, 0, SEEK_END);
    fin = ftell (pf);
    distancia = fin - inicio;
    printf ("Inicio: %li\n"
            "Fin: %li\n"
            "Distancia: %li\n",
            inicio, fin, distancia);

    fclose (pf);

    return EXIT_SUCCESS;
}
