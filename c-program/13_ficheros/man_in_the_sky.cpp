
#include <stdio.h> 
#include <stdlib.h>

#define NOMBRE "cancion.txt"
/* Función punto de entrada */
 const char * song= " \n\
 Hace ya algún tiempo que vivo sin ti\n\
 Y aún no me acostumbro, ¿Por qué voy a mentir?\n\
 Juntos acordamos mejor separarnos\n\
 Hoy sé que no puedo seguir así\n\
\n\
 Intenté olvidarte y no lo conseguí\n\
 Lleno de recuerdos, todos hablan de ti\n\
 La casa vacía, ni luz ni alegría\n\
 Estoy muerto en vida si no estás aquí\n\
\n\
 Dime que sientes lo mismo que yo\n\
 Dime que me quieres, dímelo\n\
\n\
 Cuando zarpa el amor\n\
 Navega a ciegas, es quien lleva el timón\n\
 Y cuando sube la marea al corazón\n\
 Sabe que el viento sopla a su favor\n\
 No podemos hacer nada\n\
 Por cambiar el rumbo que marcó\n\
 Para los dos\n\
 Cuando zarpa el amor\n\
 \n\
 ";
void print_usage () {
    printf ("Esto se usa así|n");
}

void informo (const char *mssg){
    print_usage ();
    fprintf (stderr, "%s\n", mssg);
    exit (1);
}

int main(int argc, char *argv[]){
    FILE *fichero;

    if ( !(fichero = fopen ( NOMBRE, "w" )) )
        informo ("No se ha podido abrir el fichero."); /*para crear un tubo para el fichero*/
    fprintf(fichero, "%s", song);
    fclose (fichero); /*para cerrar el tubo*/
    return EXIT_SUCCESS;
}
