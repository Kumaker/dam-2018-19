#include <time.h>
#include <stdio.h> 
#include <stdlib.h>

#define FILENAME "volvado.txt"
/* Función punto de entrada */
int main(int argc, char *argv[]){
   FILE *pf;
   int pos;

   srand (time (NULL));
   if ( !(pf = fopen (FILENAME, "r")) ){
    fprintf (stderr, "Aín.\n");
    return EXIT_FAILURE;
   }
   pos = rand () % 100;

   fseek (SEEK_SET, SEEK_CUR, SEEK_END);
   printf ("Pos: %i => %c\n", pos , (char) getc (pf));

   fclose(pf);

    return EXIT_SUCCESS;
}
