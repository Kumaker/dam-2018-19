
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>

#define N 0x10

/* Función punto de entrada */
int main(int argc, char *argv[]){

    char buffer[N];
    int len;
    char * frase;

    printf ("Nombre: ");
    scanf (" %s", buffer);
    len = strlen (buffer);
    frase = (char *) malloc (len + 1);
    strncpy (frase, buffer, N);
    if (len < N)
        frase[len] = '\0';
    else
        frase[N-1] = '\0';
    printf ("Tu cadena es: %s\n", frase);

    free (frase);

    return EXIT_SUCCESS;
}
