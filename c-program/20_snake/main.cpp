#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "snake.h"

void pintar (struct TSnake snake) {
    clear();
    for (int i=0; i<snake.cima; i++)
        mvprintw ( snake.anillo[i].pos.y,
                   snake.anillo[i].pos.x, "O" ); /*un printw es igual que un printf pero pinta en pantalla y un mvprintw es igual que un printw pero nos pide las coordenadas*/
    refresh (); /*volcar el array a la pantalla*/
}
/* Función punto de entrada */
int main(int argc, char *argv[]){
    struct TSnake snake;
    int input;

    initscr ();
    halfdelay (2);
    keypad (stdscr, TRUE);
    curs_set (0);
    iniciar (LINES, COLS);

    parir (&snake);

    do {
        //Mirar entradas
        input = getch ();
        if (input >= KEY_DOWN && input <= KEY_RIGHT)
        snake.anillo[0].vel = velocidades[input - KEY_DOWN];

        //Actualizar fisicas
        mover (&snake);
        if (snake.anillo[0].pos.x > COLS)
            snake.anillo[0].pos.x = 0;
        //Repintar
        pintar (snake);
    } while ( input != 0x1B );
    curs_set (1);
    endwin ();
    printf ("%X %X %X %X\n", KEY_UP, KEY_LEFT, KEY_DOWN, KEY_RIGHT);

    return EXIT_SUCCESS;
}
