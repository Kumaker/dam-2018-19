#ifndef __SNAKE_H__
#define __SNAKE_H__

#include <time.h>
#include <stdlib.h>

#define AMAX 0x200
struct TVector {
    double x;
    double y;
};

struct TAnillo {
    struct TVector pos;
    struct TVector vel;
};
struct TSnake {
    struct TAnillo anillo[AMAX];
    int cima;
    int vidas;
};


#ifdef __cplusplus
extern "C" {
#endif
    void iniciar   (int lines, int cols   );
    void parir     (struct TSnake  *snake );
    void mover     (struct TSnake  *snake );
    void crecer    (struct TSnake  *snake );
#ifdef __cplusplus
}
#endif

#endif
