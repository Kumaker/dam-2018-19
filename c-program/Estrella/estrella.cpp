#include <stdio.h>
#include <stdlib.h>
#include "estrella.h"

#define FILENAME "estrella.txt"
#define MAX 0x100

int titulo(){
	char respuesta;
	system ("clear");
	system ("toilet -f pagga ESTRELLA");
	printf("¿Quieres poner una estrella?(s/n): ");
	scanf(" %c", &respuesta);

	if(respuesta == 's'){
		return 1;
	}
	else if(respuesta == 'n')
		return 0;
	else
		titulo();
	return 0;
}

void push (struct TPila *p){
	p->cima++;
}


/* Función punto de entrada */
int main(int argc, char *argv[]) {
	struct TPila pila;
	pila.cima = 0;

	while( titulo() ){
		pila.data[pila.cima] = (struct TEstrella *) malloc(MAX * sizeof (struct TEstrella));
		printf("Dime la ascension de la estrella: ");
		scanf(" %lf", &pila.data[pila.cima]->ascension);
		printf("Dime la declinación: ");
		scanf(" %lf", &pila.data[pila.cima]->declinacion);
		printf("Dime la distancia: ");
		scanf(" %lf", &pila.data[pila.cima]->distancia);
		printf("Dime la masa: ");
		scanf(" %lf", &pila.data[pila.cima]->masa);
		printf("Dime el diametro: ");
		scanf(" %lf", &pila.data[pila.cima]->diametro);
		printf("Dime la edad: ");
		scanf(" %lf", &pila.data[pila.cima]->edad);
		printf("Dime el brillo (de 0 a 6): ");
		scanf(" %lf", &pila.data[pila.cima]->brillo);
		push(&pila);
	}

	FILE *pf;

	if(!(pf = fopen(FILENAME, "w"))){
		fprintf(stderr,"error al abrir el archivo");
		return EXIT_FAILURE;
	}

	for (int i=0; i<pila.cima; i++){
		fprintf(pf, "\
				ascension = %lf\n \
				declinacion = %lf\n \
				distancia = %lf\n \
				masa = %lf\n \
				diametro = %lf\n \
				edad = %lf\n \
				brillo = %lf\n\n",
				pila.data[i]->ascension, pila.data[i]->declinacion,
				pila.data[i]->distancia, pila.data[i]->masa,
				pila.data[i]->diametro, pila.data[i]->edad, pila.data[i]->brillo);

	}

	for(int i=0; i < pila.cima; i++)
		free(pila.data[i]);

	fclose(pf);

	return EXIT_SUCCESS;
}
