

#include <stdio.h>
#include <stdlib.h>

#define M 2
#define K 4
#define N 3

void imprime (double *m, int f, int c, const char *titulo){
    printf("\n");
    printf("%s:\n", titulo);
    for(int fila=0; fila<f; fila++){
        for(int col=0; col<c;col++)
            printf("\t%7.2lf ", *(m + fila * c + col));
        printf ("\n");
    }
}
/* Función punto de entrada */
int main(){

    int fila, col;
    double A[M][K] = {
        {2, 3, 5, 1},
        {3, 1, 4, 2}
    },
           B[K][N] = {
               {5,  2, 1},
               {3, -7, 2},
               {-4, 5, 1},
               {2, 3, -9}
           },
           C[M][N];

    for (int fila=0; fila<M; fila++)
        for(int col=0; col<N; col++){
            C[fila][col] = 0;
            for (int k=0; k<K; k++)
                C[fila][col] += A[fila][k] * B[k][col];
        }
    imprime ((double *) A, M, K, "Matriz A");
    imprime ((double *) B, K, N, "Matriz B");
    imprime ((double *) C, M, N, "Matriz C");
    printf("\n");

    return EXIT_SUCCESS;
}
