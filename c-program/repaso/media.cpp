
#include <stdio.h> 
#include <stdlib.h>
#define N 1000
/* Función punto de entrada */
int main(){
int n_alumnos = 0;
    double nota[N],
           entrada,
           media;
/*entrada de datos*/
    do{
    printf("Nota: ");
    scanf(" %lf", &entrada);
    if (entrada >=0)
        nota[n_alumnos++] = entrada;
    } while (entrada >= 0);

/*caculos*/
    for (int i=0; i<n_alumnos; i++)
        media += nota[i];

    media /= n_alumnos;

/*salida de datos*/
    printf("Media: %.2lf\n", media);

    return EXIT_SUCCESS;
}
