
#include <stdio.h> 
#include <stdlib.h>
#include <math.h>

/* Función punto de entrada */
int main(){
   
    int potencia = 5; //la potencia que queramos
    int resultado = 1; //Siempre a 1 para que tengamos una base
    int x=2; //El numero que queremos multiplicar

    /* Opcion 1*/
    for (int vez=0; vez<potencia; vez++)
        resultado *= x;

    /* Alternativa */
    pow(x,potencia);

    return EXIT_SUCCESS;
}
