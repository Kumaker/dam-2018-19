
#include <stdio.h> 
#include <stdlib.h>
#include <math.h>

#define MAX 10
/* Función punto de entrada */
int main(){

    double f = 0, potencia = 1; //Siempre a 1 para que tengamos una base
    int x=2; //El numero que queremos multiplicar

    double c[MAX] = {1,7,3,4,5};
    //  1*x^0 + 2*x^1 + 3*x^2 +4*x^3+ 5*x^4 
    // c[0]*x^0 + c[1]*x^1 + c[2]*x^2 + c[3]*x^3 + c[4]*x^4
    // c[i]*x^i
    // f += c[i]*x^i

    for(int termino=0; termino<MAX; termino++)
        potencia = 1;
        for(int exponenete=0; exponente<termino; exponenete++)
            potencia *= x;
        f += c[termino] * potencia;

printf("f(%i) = %.2lf\n",x,f);

    return EXIT_SUCCESS;
}
