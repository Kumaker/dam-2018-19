
#include <stdio.h> 
#include <stdlib.h>

#define N 10
/* Función punto de entrada */
int main(){
    int lista[N];

    for (int i=0; i<N; i++){
        lista[i] = i + 1;
        printf(" %i", lista[i]);

        printf("\n");
    }
    return EXIT_SUCCESS;
}
