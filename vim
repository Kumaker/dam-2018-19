Instala vim con el comando apt install vim
-----------------------------------------------------
Clonas el repositorio
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

se te crea una carpeta .vim y se guardara en home

Despues abres tres archivos nuevo que se llamaran
------------------------------------------------------
.vimrc que se guardara en la carpeta home con

set nu    "numeros de las lineas
set cindent   "Cuando paso a una nueva linea, el sangra solo las lineas
set sw=4  "tamaño de desplazamiento 
set expandtab "no le damos al tabulador, se utiliza para no usar el tabulador
set softtabstop=2 "Cuando le das al tabulador avanza lo que le indiques
syntax on
set splitright

source ~/.vim/vundle.vim

set t_Co=256
set syntax=on
" set background=dark
colorscheme distinguished
----------------------------------------------------------
vundle.vim que estara cuardada dentro de .vim con

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
    set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
    " alternatively, pass a path where Vundle should install plugins
    "call vundle#begin('~/some/path/here')

    " let Vundle manage Vundle, required
    Plugin 'VundleVim/Vundle.vim'


    source ~/.vim/plugins.vundle


    " All of your Plugins must be added before the following line
    call vundle#end()            " required
    filetype plugin indent on    " required
    " To ignore plugin indent changes, instead use:
    "filetype plugin on
    "
    " Brief help
    " :PluginList       - lists configured plugins
    " :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
    " :PluginSearch foo - searches for foo; append `!` to refresh local cache
    " :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
    "
    " see :h vundle for more details or wiki for FAQ
    " Put your non-Plugin stuff after this line
    ----------------------------------------------------------------------------
    y el tercero plugins.vundle ubicado tambien dentro de .vim con

    Plugin 'Raimondi/delimitMate'
    Plugin 'nathanaelkane/vim-indent-guides'
    Plugin 'Valloric/YouCompleteMe'
    Plugin 'tpope/vim-fugitive'
    Plugin 'L9'
    Plugin 'git://git.wincent.com/command-t.git'
    Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
    Plugin 'Lokaltog/vim-distinguished'
    Plugin 'altercation/vim-colors-solarized'
    Plugin 'godlygeek/tabular'
    Plugin 'marijnh/tern_for_vim'
    Plugin 'wincent/Command-T'
    Plugin 'tomtom/tcomment_vim'
    Plugin 'bling/vim-airline'
    Plugin 'bitc/vim-bad-whitespace'
    Plugin 'mattn/emmet-vim' 
    Plugin 'myusuf3/numbers.vim'
    -----------------------------------------------------------------------------
    Ejecuta VIM y lanza el comando :PluginInstall

    -----------------------------------------------------------------------------
    Entramos en la carpeta de /home/user/.vim/bundle/YouCompleteMe
      -Abrimos la terminal
      -Ejecutamos el comando git submodule update --init --recursive
      -Ejecutamos ./install.py y el ./install.sh
